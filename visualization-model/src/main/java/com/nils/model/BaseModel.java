package com.nils.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * @author tal.maayani on 5/18/2015.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "_type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ActorModel.class, name = "ActorModel"),
        @JsonSubTypes.Type(value = ApplicationModel.class, name = "ApplicationModel"),
        @JsonSubTypes.Type(value = HistogramModel.class, name = "HistogramModel"),
        @JsonSubTypes.Type(value = PerformanceModel.class, name = "PerformanceModel"),
        @JsonSubTypes.Type(value = MessageModel.class, name = "MessageModel")
})
public class BaseModel implements Serializable {
        public BaseModel() {}

}
