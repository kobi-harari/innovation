package com.nils.model;

/**
 * Created by kobi.harari on 5/21/2015.
 */
public class MessageModel extends BaseModel {
    String senderActor;
    String receiverActor;
    Long sentTime;
    Long receivedTime;
    Object message;

    public MessageModel() {}

    public MessageModel(String senderActor, String receiverActor, Long sentTime, Long receivedTime, Object message) {
        this.senderActor = senderActor;
        this.receiverActor = receiverActor;
        this.sentTime = sentTime;
        this.receivedTime = receivedTime;
        this.message = message;
    }

    public String getSenderActor() {
        return senderActor;
    }

    public void setSenderActor(String senderActor) {
        this.senderActor = senderActor;
    }

    public String getReceiverActor() {
        return receiverActor;
    }

    public void setReceiverActor(String receiverActor) {
        this.receiverActor = receiverActor;
    }

    public Long getSentTime() {
        return sentTime;
    }

    public void setSentTime(Long sentTime) {
        this.sentTime = sentTime;
    }

    public Long getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(Long receivedTime) {
        this.receivedTime = receivedTime;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MessageModel{" +
                "senderActor='" + senderActor + '\'' +
                ", receiverActor='" + receiverActor + '\'' +
                ", sentTime=" + sentTime +
                ", receivedTime=" + receivedTime +
                ", message=" + message +
                '}';
    }
}
