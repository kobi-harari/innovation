package com.nils.model.event;

import com.nils.model.MessageModel;

/**
 * Created by kobi.harari on 5/21/2015.
 * Event that will fire upon message received
 */
public class EventMessageReceived extends EventBaseModel {
    MessageModel message;

    public EventMessageReceived() {}

    public EventMessageReceived(MessageModel message) {
        this.message = message;
    }

    public MessageModel getMessage() {
        return message;
    }

    public void setMessage(MessageModel message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "EventMessageRecieved{" +
                "message=" + message +
                '}';
    }
}
