package com.nils.model.event;

import com.nils.model.HistogramModel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kobi on 6/5/15.
 * A class that holds the data for a histogram
 */
public class EventHistogram extends EventBaseModel {
    HistogramModel histogramModel;

    public EventHistogram() {}

    public void setHistogramModel(HistogramModel histogramModel) {
        this.histogramModel = histogramModel;
    }

    public EventHistogram(HistogramModel histogramModel) {
        this.histogramModel = histogramModel;
    }

    public HistogramModel getHistogramModel() {
        return histogramModel;
    }

    public Map<String, String> getHistogramData(){
        Map<String, String> data = new HashMap<>(10);
        String SEPARATOR = ".";
        String EVENT_NAME_MAX = "max";
        data.put(this.appKey + SEPARATOR + EVENT_NAME_MAX, Long.toString(this.histogramModel.getMax()));
        String EVENT_NAME_MIN = "min";
        data.put(this.appKey + SEPARATOR + EVENT_NAME_MIN, Long.toString(this.histogramModel.getMin()));
        String EVENT_NAME_MEAN = "mean";
        data.put(this.appKey + SEPARATOR + EVENT_NAME_MEAN, Double.toString(this.histogramModel.getMean()));
        String EVENT_NAME_MEDIAN = "median";
        data.put(this.appKey + SEPARATOR + EVENT_NAME_MEDIAN, Double.toString(this.histogramModel.getMedian()));
        String EVENT_NAME_PRECENTILE75 = "Percentile75";
        data.put(this.appKey + SEPARATOR + EVENT_NAME_PRECENTILE75, Double.toString(this.histogramModel.getPercentile75()));
        String EVENT_NAME_PRECENTILE95 = "Percentile95";
        data.put(this.appKey + SEPARATOR + EVENT_NAME_PRECENTILE95, Double.toString(this.histogramModel.getPercentile95()));
        String EVENT_NAME_PRECENTILE98 = "Percentile98";
        data.put(this.appKey + SEPARATOR + EVENT_NAME_PRECENTILE98, Double.toString(this.histogramModel.getPercentile98()));
        String EVENT_NAME_PRECENTILE99 = "Percentile99";
        data.put(this.appKey + SEPARATOR + EVENT_NAME_PRECENTILE99, Double.toString(this.histogramModel.getPercentile99()));
        String EVENT_NAME_PRECENTILE999 = "Percentile999";
        data.put(this.appKey + SEPARATOR + EVENT_NAME_PRECENTILE999, Double.toString(this.histogramModel.getPercentile999()));
        String EVENT_NAME_STD_DEV = "std_dev";
        data.put(this.appKey + SEPARATOR + EVENT_NAME_STD_DEV, Double.toString(this.histogramModel.getStdDev()));
        return data;
    }


    @Override
    public String toString() {
        return "EventHistorgram{" +
                "histogramModel=" + histogramModel +
                '}';
    }
}
