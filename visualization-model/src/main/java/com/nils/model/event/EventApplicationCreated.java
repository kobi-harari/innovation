package com.nils.model.event;

/**
 * Created by kobi.harari on 5/21/2015.
 * Event that represent the creation of application
 */
public class EventApplicationCreated extends EventBaseModel {
    String name;

    public EventApplicationCreated() {
    }

    public EventApplicationCreated(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "EventApplicationCreated{" +
                "name='" + name + '\'' +
                '}';
    }
}
