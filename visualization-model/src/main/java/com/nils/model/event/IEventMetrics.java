package com.nils.model.event;

/**
 * Created by kobi on 6/2/15.
 */
public interface IEventMetrics {
    Long getValue();
    String getMetricsName();
}
