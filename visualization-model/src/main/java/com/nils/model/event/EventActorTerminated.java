package com.nils.model.event;

import com.nils.model.ActorModel;

/**
 * Created by kobi.harari on 5/21/2015.
 */
public class EventActorTerminated extends EventBaseModel {
    ActorModel actor;

    public EventActorTerminated() {
    }

    public EventActorTerminated(ActorModel actor) {
        this.actor = actor;
    }

    public ActorModel getActor() {
        return actor;
    }

    public void setActor(ActorModel actor) {
        this.actor = actor;
    }

    @Override
    public String toString() {
        return "EventActorTerminated{" +
                "actor=" + actor +
                '}';
    }
}
