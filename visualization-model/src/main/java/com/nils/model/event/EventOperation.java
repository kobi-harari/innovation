package com.nils.model.event;

/**
 * Created by kobi.harari on 5/21/2015.
 */
public enum EventOperation {
    STARTED,STOPPED,MESSAGE_SENT,MESSAGE_RECIEVED
}
