package com.nils.model.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nils.model.PerformanceModel;

/**
 * Created by kobi on 6/5/15.
 */
public class EventMetrics extends EventBaseModel implements IEventMetrics {
    private PerformanceModel performanceMode;

    public EventMetrics() {}

    public EventMetrics(PerformanceModel performanceMode) {
        this.performanceMode = performanceMode;
    }

    public PerformanceModel getPerformanceMode() {
        return performanceMode;
    }

    public void setPerformanceMode(PerformanceModel performanceMode) {
        this.performanceMode = performanceMode;
    }

    @Override
    @JsonIgnore
    public Long getValue() {
        return performanceMode.getProcessingTimeMicro();
    }

    @Override
    @JsonIgnore
    public String getMetricsName() {
        return performanceMode.getClassName() + "." + performanceMode.getActorPath();
    }

    @Override
    public String toString() {
        return "EventMetrics{" +
                "performanceMode=" + performanceMode +
                '}';
    }

}
