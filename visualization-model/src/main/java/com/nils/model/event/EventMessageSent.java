package com.nils.model.event;

import com.nils.model.MessageModel;

/**
 * Created by kobi.harari on 5/21/2015.
 * Event that will be fired upon message sent
 */
public class EventMessageSent extends EventBaseModel {
    MessageModel message;

    public EventMessageSent (MessageModel message) {
        this.message = message;
    }

    public MessageModel getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "EventMessageRecieved{" +
                "message=" + message +
                '}';
    }
}
