package com.nils.model.event;

import com.nils.model.ActorModel;

/**
 * @author tal.maayani on 5/21/2015.
 */
public class EventActorCreated extends EventBaseModel {
    ActorModel actor;

    public EventActorCreated() {}

    public EventActorCreated(ActorModel actor) {
        this.actor = actor;
    }

    public ActorModel getActor() {
        return actor;
    }

    public void setActor(ActorModel actor) {
        this.actor = actor;
    }

    @Override
    public String toString() {
        return "EventActorCreated{" +
                "actor=" + actor +
                '}';
    }
}
