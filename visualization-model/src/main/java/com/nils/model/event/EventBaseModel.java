package com.nils.model.event;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * @author tal.maayani on 5/21/2015.
 */

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "_type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = EventActorCreated.class, name = "EventActorCreated"),
        @JsonSubTypes.Type(value = EventActorTerminated.class, name = "EventActorTerminated"),
        @JsonSubTypes.Type(value = EventApplicationCreated.class, name = "EventApplicationCreated"),
        @JsonSubTypes.Type(value = EventMessageSent.class, name = "EventMessageSent"),
        @JsonSubTypes.Type(value = EventMessageReceived.class, name = "EventMessageReceived"),
        @JsonSubTypes.Type(value = EventMetrics.class, name = "EventMetrics"),
        @JsonSubTypes.Type(value = EventHistogram.class, name = "EventHistogram"),
        @JsonSubTypes.Type(value = EventRouterDiscovered.class, name = "EventRouterDiscovered"),
})
public class EventBaseModel implements Serializable {
    long eventDate;
    long sequence;
    String appKey;

    public EventBaseModel() {
    }

    public long getEventDate() {
        return eventDate;
    }

    public void setEventDate(long eventDate) {
        this.eventDate = eventDate;
    }

    public long getSequence() {
        return sequence;
    }

    public void setSequence(long sequence) {
        this.sequence = sequence;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    @Override
    public String toString() {
        return "EventBaseModel{" +
                "eventDate=" + eventDate +
                ", sequence=" + sequence +
                ", appKey='" + appKey + '\'' +
                '}';
    }
}
