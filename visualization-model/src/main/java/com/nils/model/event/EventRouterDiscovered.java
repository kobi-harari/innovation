package com.nils.model.event;

/**
 * Created by kobi on 7/14/15.
 */
public class EventRouterDiscovered extends EventBaseModel {
    String routerPath;
    String parentPath;

    public EventRouterDiscovered() {
    }

    public EventRouterDiscovered(String routerPath, String parentPath) {
        this.routerPath = routerPath;
        this.parentPath = parentPath;
    }

    public String getRouterPath() {
        return routerPath;
    }

    public void setRouterPath(String routerPath) {
        this.routerPath = routerPath;
    }

    public String getParentPath() {
        return parentPath;
    }

    public void setParentPath(String parentPath) {
        this.parentPath = parentPath;
    }
}
