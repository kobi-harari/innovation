package com.nils.model;

/**
 * Created by kobi on 5/23/15.
 */
public class HistogramModel extends BaseModel{
    private long max;
    private long min;
    private double mean;
    private double median;
    private double stdDev;
    private double percentile75;
    private double percentile95;
    private double percentile98;
    private double percentile99;
    private double percentile999;

    public HistogramModel() {}

    public void setMax(long max) {
        this.max = max;
    }

    public void setMin(long min) {
        this.min = min;
    }

    public void setMean(double mean) {
        this.mean = mean;
    }

    public void setMedian(double median) {
        this.median = median;
    }

    public void setStdDev(double stdDev) {
        this.stdDev = stdDev;
    }

    public void setPercentile75(double percentile75) {
        this.percentile75 = percentile75;
    }

    public void setPercentile95(double percentile95) {
        this.percentile95 = percentile95;
    }

    public void setPercentile98(double percentile98) {
        this.percentile98 = percentile98;
    }

    public void setPercentile99(double percentile99) {
        this.percentile99 = percentile99;
    }

    public void setPercentile999(double percentile999) {
        this.percentile999 = percentile999;
    }

    public long getMax() {
        return max;
    }

    public long getMin() {
        return min;
    }

    public double getMean() {
        return mean;
    }

    public double getMedian() {
        return median;
    }

    public double getStdDev() {
        return stdDev;
    }

    public double getPercentile75() {
        return percentile75;
    }

    public double getPercentile95() {
        return percentile95;
    }

    public double getPercentile98() {
        return percentile98;
    }

    public double getPercentile99() {
        return percentile99;
    }

    public double getPercentile999() {
        return percentile999;
    }

    @Override
    public String toString() {
        return "HistogramModel{" +
                "max=" + max +
                ", min=" + min +
                ", mean=" + mean +
                ", median=" + median +
                ", stdDev=" + stdDev +
                ", percentile75=" + percentile75 +
                ", percentile95=" + percentile95 +
                ", percentile98=" + percentile98 +
                ", percentile99=" + percentile99 +
                ", percentile999=" + percentile999 +
                '}';
    }
}
