package com.nils.model;

import java.util.HashSet;
import java.util.Set;

/**
 * @author tal.maayani on 5/14/2015.
 */
public class ActorModel extends BaseModel {
    String instanceName;
    String className;
    String path;
    Long terminationTime;
    Long startTime;
    String parentKey = null;
    Set<String> children = new HashSet<>();

    public ActorModel() {
    }

    public ActorModel(String instanceName) {
        this.instanceName = instanceName;
        this.path = instanceName;
        startTime = System.currentTimeMillis();
    }

    public ActorModel(String instanceName, String className, String path, Long terminationTime, Long startTime, String parent) {
        this.instanceName = instanceName;
        this.className = className;
        this.path = path;
        this.terminationTime = terminationTime;
        this.startTime = startTime;
        this.parentKey = parent;
    }

    public ActorModel(String instanceName, String className, String path, Long startTime, Long terminationTime) {
        this.instanceName = instanceName;
        this.className = className;
        this.path = path;
        this.startTime = startTime;
        this.terminationTime = terminationTime;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getTerminationTime() {
        return terminationTime;
    }

    public void setTerminationTime(Long terminationTime) {
        this.terminationTime = terminationTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public String getParentKey() {
        return parentKey;
    }

    public void setParentKey(String parentKey) {
        this.parentKey = parentKey;
    }

    public void addChildren(String name) {
        this.children.add(name);
    }

    public void removeChildren(String name) {
        this.children.remove(name);
    }

    public Set<String> getChildren() {
        return children;
    }

    public void setChildren(Set<String> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "ActorModel{" +
                "instanceName='" + instanceName + '\'' +
                ", className='" + className + '\'' +
                ", path='" + path + '\'' +
                ", startTime=" + startTime +
                ", terminationTime=" + terminationTime +
                ", parentKey=" + parentKey +
                '}';
    }
}
