package com.nils.model;

/**
 * Created by kobi.harari on 5/25/2015.
 * class that represent raw data of message
 */
public class PerformanceModel extends BaseModel {
    private String actorPath;
    private String className;
    Long processingTimeMicro;

    public PerformanceModel() {
    }

    public PerformanceModel(String actorPath, String className, Long processingTimeMicro) {
        this.actorPath = actorPath;
        this.className = className;
        this.processingTimeMicro = processingTimeMicro;
    }

    public String getActorPath() {
        return actorPath;
    }

    public String getClassName() {
        return className;
    }

    public Long getProcessingTimeMicro() {
        return processingTimeMicro;
    }

    public void setActorPath(String actorPath) {
        this.actorPath = actorPath;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void setProcessingTimeMicro(Long processingTimeMicro) {
        this.processingTimeMicro = processingTimeMicro;
    }

    @Override
    public String toString() {
        return "EventPerformance{" +
                "actorPath='" + actorPath + '\'' +
                ", className='" + className + '\'' +
                ", processingTimeMicro=" + processingTimeMicro +
                '}';
    }
}
