package com.nils.model;

import com.nils.model.metric.BaseMetric;

import java.util.List;

/**
 * @author tal.maayani on 5/14/2015.
 */
public class ApplicationModel extends BaseModel {
    String name;
    protected String key;
    long startSequence;
    long sequence;

    public ApplicationModel() {}

    public ApplicationModel(String name, String key) {
        this.name = name;
        this.key = key;
    }

    public ApplicationModel(String key) {
        this.key = key;
    }

    List<? extends BaseMetric> metrics;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public List<? extends BaseMetric> getMetrics() {
        return metrics;
    }

    public void setMetrics(List<? extends BaseMetric> metrics) {
        this.metrics = metrics;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getStartSequence() {
        return startSequence;
    }

    public void setStartSequence(long startSequence) {
        this.startSequence = startSequence;
    }

    public long getSequence() {
        return sequence;
    }

    public void setSequence(long sequence) {
        this.sequence = sequence;
    }
}
