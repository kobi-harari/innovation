package com.nils.example;

/**
 * Created by kobi on 5/15/15.
 */
public class MainMessage extends DispatcherMessage {
    public MainMessage(String name, String message) {
        super(name, message);
    }
}
