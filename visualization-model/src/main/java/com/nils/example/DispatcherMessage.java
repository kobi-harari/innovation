package com.nils.example;

/**
 * Created by kobi on 5/15/15.
 */
public class DispatcherMessage extends WorkerMessage {
    public DispatcherMessage(String name, String message) {
        super(name, message);
    }
}
