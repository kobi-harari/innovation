package com.nils.example;

import java.io.Serializable;

/**
 * Created by kobi on 5/15/15.
 */
public class WorkerMessage implements Serializable {
    String name;
    String message;

    public WorkerMessage(String name, String message) {
        this.name = name;
        this.message = message;
    }

    @Override
    public String toString() {
        return "WorkerMessage{" +
                "name='" + name + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
