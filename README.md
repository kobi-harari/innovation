# README #

For Nils Project

### Nils Monitor your java/akka application ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* download the repository
* there are 4 subproject
* run the nils server in the *visualization-server*
* in the example project - run one of the main files that create actor systems (TestRandomActorSystem, TestSimpleActorSystem)
* connect the UI to the nils server use the following configuration:
```
#!javascript
akka{
  monitoring {
    Graphite{
    host = "graphite.dev.sizmdx.com"
    port = 2003
    api_key = ""
    }
    Nils {
    path = "/event"
    protocol = "http"
    host = "localhost"
    port = 9898
    }
}

```
## make sure that you add a `-javaagent:/Development/tools/aspect/aspectjweaver.jar` the running field ##

####  how to use apo
1. make sure that you add the javaagent to the jvm parameters for the weaving 
2. you need to add a new folder in the resources folder named META-INF and add the aop.xml to it
  * example of a aop.xml
  ```xml
<aspectj>
    <aspects>
        <aspect name="com.sizmek.analytics.rb.aspect.LoggerAspect"/>
    </aspects>
    <!--<weaver options="-verbose -showWeaveInfo -debug">-->
    <weaver>
        <include within="akka.actor.AbstractActor.*"/>
        <include within="com.sizmek.analytics.rb.aspect.LoggerAspect"/>
    </weaver>
</aspectj>
```
  * note the aspect class must appear in the apsects section as well in the weaver section
  * also make sure that the place you want the pointcut to work needs to be added to the waever section 
3. example of a java aspect 
```java
@Aspect
public class LoggerAspect {
    private final Logger log = LoggerFactory.getLogger(LoggerAspect.class);

//    @Around("@annotation(com.sizmek.analytics.rb.annotation.InitMdc) && execution(* com.sizmek.analytics..*.*(..))")
//    @Around("execution(* akka.actor.AbstractActor.Receive.*(..))")
    @Around("execution(* com.sizmek.analytics.rb.akka.actor.ExporterManagerActor.Receive.*(..))")
    public Object initLogger(ProceedingJoinPoint joinPoint) throws Throwable {
        Object target = joinPoint.getTarget();
        InitMdc annotation = target.getClass().getAnnotation(InitMdc.class);
        if (!StringUtils.isEmpty(annotation)) {
            String memberName = annotation.value();
            try {
                ReportTemplateExecution reportTemplateExecution = (ReportTemplateExecution) FieldUtils.readField(joinPoint.getTarget(), memberName, true);
                if (reportTemplateExecution != null) {
                    System.out.println(reportTemplateExecution.getExecutionID());

                }
            } catch (IllegalAccessException | IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }finally {
                return joinPoint.proceed();
            }
        }
        return joinPoint.proceed();
    }
}
```

 