package com.nils.example.simple;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinPool;

import java.util.Random;

/**
 * Created by kobi on 5/14/15.
 */
public class TestSimpleActor1 extends UntypedActor {
    Random random = new Random();
    private ActorRef router;

    @Override
    public void onReceive(Object message) throws Exception {
        Thread.sleep(random.nextInt(1000));
        if (message.toString().startsWith("Message")) {
            router = getContext().actorOf(Props.create(TestSimpleActor3.class)
                    .withRouter(new RoundRobinPool(3)));
            TestSimpleActorSystem.actorRefs.add(router);
            router.tell("Message " + random.nextInt(), getSelf());
            router.tell("Message " + random.nextInt(), getSelf());
            router.tell("Message " + random.nextInt(), getSelf());
        } else if (router != null) {
            router.tell("Message " + random.nextInt(), router);
        }
    }
}
