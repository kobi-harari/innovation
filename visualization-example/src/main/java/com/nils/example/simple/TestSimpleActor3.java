package com.nils.example.simple;

import akka.actor.UntypedActor;

import java.util.Random;

/**
 * Created by kobi on 5/14/15.
 */
public class TestSimpleActor3 extends UntypedActor {
    Random random = new Random();

    @Override
    public void onReceive(Object message) throws Exception {
        Thread.sleep(random.nextInt(1000));
    }
}
