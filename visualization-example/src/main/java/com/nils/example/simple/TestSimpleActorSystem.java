package com.nils.example.simple;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Random;

/**
 * Created by kobi on 5/14/15.
 * -javaagent:D:\\dev\\tools\\aspect\\aspectjweaver.jar
 */
public class TestSimpleActorSystem {
    ActorSystem system = null;

    public static List<ActorRef> actorRefs = Lists.newArrayList();
    public static List<Class<?>> actorTypes = Lists.newLinkedList();

    public static void main(String[] args) {
        TestSimpleActorSystem testRandomActor = new TestSimpleActorSystem();
        testRandomActor.initActorSystem();
    }

    public void initActorSystem() {
        System.out.println("-----------------------------------");
        System.out.println("--------Init Actor System ---------");
        System.out.println("-----------------------------------");
        String name = "SimpleActorSystem";
        if (system != null) {
            System.out.println("Actor system was already initialized! Ignore duplicate initializations.");
        } else {
            system = ActorSystem.create(name);
            System.out.println(String.format("ActorSystem with name '%s', was created successfully", name));
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            startTestActor();
        }
    }

    private void startTestActor() {
        System.out.println("start test actor");
        Random random = new Random();
        actorTypes.add(TestSimpleActor1.class);
        actorTypes.add(TestSimpleActor2.class);

        for (int i = 0; i < actorTypes.size(); i++) {
            ActorRef actorRef = system.actorOf(Props.create(actorTypes.get(i)));
            actorRefs.add(actorRef);
        }

        int counter = 0;
        while (true) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            ActorRef receiver = actorRefs.get(counter % 2);
            if (counter < 2) {
                receiver.tell("Message" + counter, receiver);
            }
            receiver.tell("Propagate" + counter++, receiver);
            counter++;
        }
    }
}
