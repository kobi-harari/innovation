package com.nils.example;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.nils.akka.MainActor;

/**
 * Created by kobi on 5/15/15.
 */
public class ExampleMain {
    ActorSystem system = null;
    public static void main(String[] args) {
        ExampleMain exampleMain = new ExampleMain();
        exampleMain.initActorSystem();
        new Thread(new MessageGenerator(exampleMain.createMainActor())).run();
    }

    public void initActorSystem() {
        System.out.println("-----------------------------------");
        System.out.println("--------Init Actor System ---------");
        System.out.println("-----------------------------------");
        String name = "ExampleActorSystem";
        if (system != null) {
            System.out.println("Actor system was already initialized! Ignore duplicate initializations.");
        } else {
            system = ActorSystem.create(name);
            System.out.println(String.format("ActorSystem with name '%s', was created successfully", name));
            startTestActor();
        }
    }

    private void startTestActor(){
        System.out.println("start test actor");
//        ActorRef actorRef = system.actorOf(Props.create(TestActor.class));

    }

    private ActorRef createMainActor(){
        return system.actorOf(Props.create(MainActor.class));
    }
}
