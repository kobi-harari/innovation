package com.nils.example;

import com.google.common.collect.Lists;
import com.nils.example.random.TestRandomActorSystem;
import com.nils.example.simple.TestSimpleActorSystem;

import java.util.List;

/**
 * @author tal.maayani on 7/15/2015.
 */
public class AllExamples {
    public static void main(String[] args) {
        List<Thread> threadList = Lists.newArrayList();
        threadList.add( new Thread(() -> TestSimpleActorSystem.main(args)));
        threadList.add( new Thread(() -> TestRandomActorSystem.main(args)));

        threadList.forEach(Thread::start);
    }
}
