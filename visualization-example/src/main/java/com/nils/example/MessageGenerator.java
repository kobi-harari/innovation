package com.nils.example;

import akka.actor.ActorRef;

/**
 * Created by kobi on 5/15/15.
 * A Class for generating random messages to the dispatcher as an example
 */
public class MessageGenerator implements Runnable {
    ActorRef mainActor;

    public MessageGenerator(ActorRef mainActor) {
        this.mainActor = mainActor;
    }

    @Override
    public void run() {
        while (true) {
            System.out.println("about to send a message");
            MainMessage mainMessage = new MainMessage("main","this is a main message");
            mainActor.tell(mainMessage,null);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {

                e.printStackTrace();
            }
        }
    }
}
