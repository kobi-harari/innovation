package com.nils.example.random;

import akka.actor.*;

import java.util.Random;

/**
 * Created by kobi on 5/14/15.
 */
public class TestActor4 extends UntypedActor {
    Random random = new Random();

    @Override
    public void onReceive(Object message) throws Exception {
        int i = random.nextInt(10);
        if (i == 5) {
            TestRandomActorSystem.actorRefs.get(random.nextInt(TestRandomActorSystem.actorRefs.size())).tell(PoisonPill.getInstance(), getSelf());
        }
        if (i < 3) {
            ActorRef actorRef = getContext().actorOf(Props.create(TestRandomActorSystem.actorTypes.get(random.nextInt(TestRandomActorSystem.actorTypes.size()))));
            TestRandomActorSystem.actorRefs.add(actorRef);
        } else {
            ActorSelection actorSelection = getContext().actorSelection(getSelf().path() + "/*");
            actorSelection.tell("Message " + random.nextInt(), getSelf());        }
    }
}
