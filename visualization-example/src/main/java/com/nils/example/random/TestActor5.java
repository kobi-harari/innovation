package com.nils.example.random;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinPool;

import java.util.Random;

/**
 * Created by kobi on 5/14/15.
 */
public class TestActor5 extends UntypedActor {
    Random random = new Random();

    @Override
    public void onReceive(Object message) throws Exception {
        int i = random.nextInt(10);
        if (i == 5) {
            TestRandomActorSystem.actorRefs.get(random.nextInt(TestRandomActorSystem.actorRefs.size())).tell(PoisonPill.getInstance(), getSelf());
        }
        if (i < 3) {
            ActorRef actorRef = getContext().actorOf(Props.create(TestRandomActorSystem.actorTypes.get(random.nextInt(TestRandomActorSystem.actorTypes.size())))
                    .withRouter(new RoundRobinPool(random.nextInt(10))));
            TestRandomActorSystem.actorRefs.add(actorRef);
        } else {
            TestRandomActorSystem.actorRefs.get(random.nextInt(TestRandomActorSystem.actorRefs.size())).tell("Message " + random.nextInt(), getSelf());
        }
    }
}
