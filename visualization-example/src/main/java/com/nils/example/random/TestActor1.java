package com.nils.example.random;

import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.actor.UntypedActor;

import java.util.Random;

/**
 * Created by kobi on 5/14/15.
 */
public class TestActor1 extends UntypedActor {
    Random random = new Random();

    @Override
    public void onReceive(Object message) throws Exception {
        int i = random.nextInt(10);
        if (i < 6) {
            getContext().actorOf(Props.create(TestRandomActorSystem.actorTypes.get(random.nextInt(TestRandomActorSystem.actorTypes.size()))));
            getContext().actorOf(Props.create(TestRandomActorSystem.actorTypes.get(random.nextInt(TestRandomActorSystem.actorTypes.size()))));
        } else {
            ActorSelection actorSelection = getContext().actorSelection(getSelf().path() + "/*");
            actorSelection.tell("Message " + random.nextInt(), getSelf());
        }
    }
}
