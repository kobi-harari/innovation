package com.nils.example.random;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Random;

/**
 * Created by kobi on 5/14/15.
 * -javaagent:D:\\dev\\tools\\aspect\\aspectjweaver.jar
 */
public class TestRandomActorSystem {
    ActorSystem system = null;

    public static List<ActorRef> actorRefs = Lists.newArrayList();
    public static List<Class<?>> actorTypes = Lists.newLinkedList();
    public static ActorRef router;
    int poolSize = 100;

    public static void main(String[] args) {
        TestRandomActorSystem testRandomActor = new TestRandomActorSystem();
        testRandomActor.initActorSystem();
    }

    public void initActorSystem() {
        System.out.println("-----------------------------------");
        System.out.println("--------Init Actor System ---------");
        System.out.println("-----------------------------------");
        String name = "RandomActorSystem";
        if (system != null) {
            System.out.println("Actor system was already initialized! Ignore duplicate initializations.");
        } else {
            system = ActorSystem.create(name);
            System.out.println(String.format("ActorSystem with name '%s', was created successfully", name));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            startTestActor();
        }
    }

    private void startTestActor() {
        System.out.println("start test actor");
        Random random = new Random();
        actorTypes.add(TestActor1.class);
        actorTypes.add(TestActor2.class);
        actorTypes.add(TestActor3.class);
//        actorTypes.add(TestActor4.class);
//        actorTypes.add(TestActor5.class);
//        actorTypes.add(TestActor6.class);

        for (int i = 0; i < actorTypes.size(); i++) {
            ActorRef actorRef = system.actorOf(Props.create(actorTypes.get(0)));
            actorRefs.add(actorRef);
        }

        int counter = 0;
        while (true) {
            ActorRef sender = actorRefs.get(random.nextInt(actorRefs.size()));
            ActorRef receiver = actorRefs.get(random.nextInt(actorRefs.size()));
            if (counter < 100) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                receiver.tell("Message" + counter, sender);
            }
            receiver.tell("Propagate" + counter, sender);
            counter++;
        }


    }
}
