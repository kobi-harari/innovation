package com.nils.akka;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.nils.example.DispatcherMessage;
import com.nils.example.WorkerMessage;

import java.util.stream.IntStream;

/**
 * Created by kobi on 5/15/15.
 */
public class MainActor extends UntypedActor {
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    final ActorRef dispatcher = getContext().actorOf(Props.create(DispatcherActor.class));

    @Override
    public void onReceive(Object message) throws Exception {
        log.debug("got message: " + message);
        if (message instanceof DispatcherMessage){
            log.info("Got dispatcher message: dispatching");
            IntStream.range(0,10).forEach(i -> {
                log.debug("sending message " + i);
                dispatcher.tell(new WorkerMessage("worker","go to work"),getSelf());
            });

        }

    }
}
