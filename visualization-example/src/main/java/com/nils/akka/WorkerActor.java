package com.nils.akka;

import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

/**
 * Created by kobi on 5/15/15.
 * A worker Actor
 */
public class WorkerActor extends UntypedActor {
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    @Override
    public void onReceive(Object message) throws Exception {
        log.debug("a message was received: " + message);
        // todo - some computation here
    }
}
