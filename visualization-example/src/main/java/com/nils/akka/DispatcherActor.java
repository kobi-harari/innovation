package com.nils.akka;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.ActorRefRoutee;
import akka.routing.RoundRobinRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;
import com.nils.example.WorkerMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kobi on 5/15/15.
 * An Actor that dispatches messages
 */
public class DispatcherActor extends UntypedActor {
    Router router;
    {
        List<Routee> routees = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            ActorRef r = getContext().actorOf(Props.create(WorkerActor.class));
            getContext().watch(r);
            routees.add(new ActorRefRoutee(r));
        }
        router = new Router(new RoundRobinRoutingLogic(), routees);
    }

    @Override
    public void onReceive(Object message) throws Exception {
        router.route(new WorkerMessage("worker","go to work!"),getSelf());
    }
}
