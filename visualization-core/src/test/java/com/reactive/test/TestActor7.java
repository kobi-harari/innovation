package com.reactive.test;

import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinPool;
import com.reactive.TestMainRouter;

/**
 * Created by kobi on 7/14/15.
 */
public class TestActor7 extends UntypedActor {

    @Override
    public void onReceive(Object message) throws Exception {
        if (TestMainRouter.router == null){
            int numberOfInstances = 3;
            TestMainRouter.router = getContext().system().actorOf(Props.create(TestActor8.class).withRouter(new RoundRobinPool(numberOfInstances)));

        }
        TestMainRouter.router.tell("Hello World", this.sender());

    }
}
