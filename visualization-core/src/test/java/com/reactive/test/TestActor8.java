package com.reactive.test;

import akka.actor.UntypedActor;

/**
 * Created by kobi on 7/14/15.
 */
public class TestActor8 extends UntypedActor {

    @Override
    public void onReceive(Object message) throws Exception {
        System.out.println(message);
    }
}
