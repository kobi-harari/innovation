package com.nils.akka;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.routing.ActorRefRoutee;
import akka.routing.RoundRobinRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;
import com.nils.reporting.CombinedEventReport;
import com.nils.reporting.EventGraphiteReporter;
import com.nils.reporting.IEventReporter;
import com.nils.reporting.NilsReporter;
import com.typesafe.config.Config;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kobi.harari on 5/21/2015.
 * The Gateway to send all the messages from the agent
 */
public class MessageGatewayActor extends UntypedActor {
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    Router router;
    {
        List<Routee> routees = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ActorRef r = getContext().actorOf(Props.create(MessageSenderActor.class,createEventReporter(context().system().settings().config())));
            getContext().watch(r);
            routees.add(new ActorRefRoutee(r));
        }
        log.debug("creating the router that will handle the load of the message sending");
        router = new Router(new RoundRobinRoutingLogic(), routees);
    }

    @Override
    public void onReceive(Object message) throws Exception {
        log.debug("Got message to send: " + message);
        router.route(message, getSelf());
    }

    private IEventReporter createEventReporter(Config config){
        if (config.hasPath("akka.monitoring.Nils") && config.hasPath("akka.monitoring.Graphite")){
            log.debug("create the combine reporter");
            return new CombinedEventReport(createNilsReporter(config), createGraphiteReporter(config));
        }
        if (config.hasPath("akka.monitoring.Nils")){
            log.debug("create the nils reporter");
            return createNilsReporter(config);
        }
        if (config.hasPath("akka.monitoring.Graphite")){
            log.debug("create the graphite reporter");
            return createGraphiteReporter(config);
        }
        log.error("Reporter was not created!");
        throw new RuntimeException("Reporter was not created! working is worthless!");
    }

    private EventGraphiteReporter createGraphiteReporter(Config config){
        String host = config.getString("akka.monitoring.Graphite.host");
        if(host == null || host.isEmpty()){
            log.error("I can't initiate the reporter - host is missing, working is worthless!");
            throw new RuntimeException("I can't initiate the reporter - host is missing, working is worthless!");
        }
        int port = config.getInt("akka.monitoring.Graphite.port");
        if(port == 0){
            log.error("I can't initiate the reporter - port is missing, working is worthless!");
            throw new RuntimeException("I can't initiate the reporter - port is missing, working is worthless!");
        }
        String apiKey = config.getString("akka.monitoring.Graphite.api_key");
        if (apiKey == null && apiKey.isEmpty()){
            log.error("There is no api key ");
            apiKey = "";
        }
        log.info("Graphite reporter initialized for this monitoring");
        return new EventGraphiteReporter(host,port,apiKey);
    }

    private NilsReporter createNilsReporter(Config config){
        String host = config.getString("akka.monitoring.Nils.host");
        if(host == null || host.isEmpty()){
            log.error("I can't initiate the reporter - host is missing, working is worthless!");
            throw new RuntimeException("I can't initiate the reporter - host is missing, working is worthless!");
        }
        int port = config.getInt("akka.monitoring.Nils.port");
        if(port == 0){
            log.error("I can't initiate the reporter - port is missing, working is worthless!");
            throw new RuntimeException("I can't initiate the reporter - port is missing, working is worthless!");
        }
        String path = config.getString("akka.monitoring.Nils.path");
        if(path == null || path.isEmpty()){
            log.error("I can't initiate the reporter - path is missing, working is worthless!");
            throw new RuntimeException("I can't initiate the reporter - path is missing, working is worthless!");
        }
        String protocol = config.getString("akka.monitoring.Nils.protocol");
        if(protocol == null || protocol.isEmpty()){
            log.error("I can't initiate the reporter - protocol is missing, working is worthless!");
            throw new RuntimeException("I can't initiate the reporter - protocol is missing, working is worthless!");
        }
        return new NilsReporter(host, port,protocol, path);
    }
}
