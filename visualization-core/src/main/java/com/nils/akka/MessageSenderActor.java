package com.nils.akka;

import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.nils.model.event.EventBaseModel;
import com.nils.model.event.EventHistogram;
import com.nils.model.event.EventMetrics;
import com.nils.reporting.IEventReporter;

/**
 * Created by kobi.harari on 5/21/2015.
 * An actor that only sends messages
 */
public class MessageSenderActor extends UntypedActor {
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    IEventReporter reporter;

    public MessageSenderActor(IEventReporter reporter) {
        this.reporter = reporter;
        log.debug("MessageSenderActor created");
    }

    @Override
    public void onReceive(Object message) throws Exception {
        log.debug("about to send a message from the agent");
        if (message instanceof EventMetrics){
            log.debug("about to report metrics event");
            reporter.reportMetricsEvent((EventMetrics) message);
        } else if (message instanceof EventHistogram){
            log.debug("about to report histogram event");
            reporter.reportHistogramEvent((EventHistogram) message);
        } else if (message instanceof EventBaseModel){
            log.debug("about to report other event");
            reporter.reportEvent(message);
        }
    }
}
