package com.nils.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.nils.model.event.EventBaseModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by kobi.harari on 5/21/2015.
 * Akka manager that will handle all messages that will be published to the server (either Nils or Graphite)
 */
public class AkkaManager {
    private static final Logger log = LoggerFactory.getLogger(AkkaManager.class);
    ActorRef messageGateway;
    private String applicationName;
    public static AkkaManager instance = new AkkaManager();
    private AtomicLong counter;

    private AkkaManager() {

    }

    public void initAkkaManager(ActorSystem actorSystem) {
        log.debug("initAkkaManager started");
        this.messageGateway = actorSystem.actorOf(Props.create(MessageGatewayActor.class));
        this.applicationName = actorSystem.name();
        this.counter = new AtomicLong(0);
    }

    public void sendMessage(EventBaseModel message) {
        applyBaseData(message);
        log.debug("About to send the following message: " + message);
        this.messageGateway.tell(message, null);
    }

    private void applyBaseData(EventBaseModel message) {
        message.setAppKey(this.applicationName);
        message.setSequence(counter.incrementAndGet());
        message.setEventDate(System.currentTimeMillis());
    }

}