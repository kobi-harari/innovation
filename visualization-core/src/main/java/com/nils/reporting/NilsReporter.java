package com.nils.reporting;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.nils.model.event.EventBaseModel;
import com.nils.model.event.EventHistogram;
import com.nils.model.event.EventMetrics;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URISyntaxException;

/**
 * @author tal.maayani on 5/17/2015.
 *         Repor Events to the destination according to the configuration that was handeled
 */
public class NilsReporter implements IEventReporter{
    Logger log = LoggerFactory.getLogger(NilsReporter.class);

    private WebResource resource;
    private String host;
    private Integer port;
    private String protocol;
    private String path;

    private static Gson gson = new Gson();

    public NilsReporter(String host, int port, String protocol, String path) {
        this.host = host;
        this.port = port;
        this.protocol = protocol;
        this.path = path;
        initClient();
    }

    private void initClient() {
        Client client = Client.create();
        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setHost(this.host);
        uriBuilder.setPort(this.port);
        uriBuilder.setPath(this.path);
        uriBuilder.setScheme(this.protocol);
        try {
            log.debug("SERVER URL: {}",uriBuilder.build().toString());
            this.resource = client.resource(uriBuilder.build());
        } catch (URISyntaxException e) {
            log.error("Something is wrong!", e);
        }

    }

    public void reportEvent(Object event) {
        reportInternal(event);
    }

    @Override
    public void reportHistogramEvent(EventHistogram eventHistogram) {
        reportInternal(eventHistogram);
    }

    @Override
    public void reportMetricsEvent(EventMetrics eventMetrics) {
        reportInternal(eventMetrics);
    }

    private void reportInternal(Object event){
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        String toSend = null;
        try {
            toSend = mapper.writeValueAsString(event);
        } catch (JsonProcessingException e) {
            log.error("could not map to json.", e);
        }

        log.debug("Reporting event: " + toSend);
        this.resource.header("Content-Type", "application/json").post(toSend);
    }
}
