package com.nils.reporting;

import com.codahale.metrics.graphite.Graphite;
import com.nils.model.event.EventBaseModel;
import com.nils.model.event.EventHistogram;
import com.nils.model.event.EventMetrics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;

/**
 * Created by kobi on 6/2/15.
 * class that will report to graphite
 */
public class EventGraphiteReporter implements IEventReporter {
    private static final Logger log = LoggerFactory.getLogger(EventGraphiteReporter.class);
    private static final String GRAPHITE_SEPARATOR = ".";
    Graphite graphite;
    private String apiKey;

    public EventGraphiteReporter(String host, Integer port, String apiKey) {
        this.graphite = new Graphite(new InetSocketAddress(host, port));
        this.apiKey = apiKey;
    }


    @Override
    public void reportEvent(Object event) {
        // do nothing graphite can't really do anything with other messages since graphite can't do anything with them
    }

    @Override
    public void reportHistogramEvent(EventHistogram eventHistogram) {
        sendMessage(eventHistogram.getHistogramData(), (System.currentTimeMillis() / 1000));
    }

    @Override
    public void reportMetricsEvent(EventMetrics eventMetrics) {
        String metricsName = eventMetrics.getAppKey() + "." + eventMetrics.getMetricsName();
        Long value = eventMetrics.getValue();
        sendMessage(metricsName, value.toString(), (System.currentTimeMillis() / 1000));
    }

    private void sendMessage(Map<String, String> data, long timestamp) {
        data.forEach((k, v) -> sendMessage(k, v, timestamp));
    }


    private void sendMessage(String key, String value, long timestamp) {
        try {
            this.graphite.connect();
        } catch (IOException e) {
            log.error("Could not connect to Graphite server");
        }
        try {
            this.graphite.send(key, value, timestamp);
        } catch (IOException e) {
            log.error("could not send the message, something is wrong", e);
        } finally {
            try {
                this.graphite.close();
            } catch (IOException e) {
                log.error("Could not close the connection to Graphite", e);
            }
        }
    }

//    private String assembleKey(String key) {
//        if (key != null && !key.isEmpty()){
//            return this.apiKey + GRAPHITE_SEPARATOR + key;
//        }
//        return key;
//    }
}
