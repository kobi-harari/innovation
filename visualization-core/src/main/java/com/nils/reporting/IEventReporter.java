package com.nils.reporting;

import com.nils.model.event.EventHistogram;
import com.nils.model.event.EventMetrics;

/**
 * Created by kobi on 6/2/15.
 */
public interface IEventReporter {
    void reportEvent(Object event);
    void reportHistogramEvent(EventHistogram eventHistogram);
    void reportMetricsEvent(EventMetrics eventMetrics);
}
