package com.nils.reporting;

import com.nils.model.event.EventHistogram;
import com.nils.model.event.EventMetrics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by kobi.harari on 7/15/2015.
 * If you want to report to two data sinks use the combine event reporter
 */
public class CombinedEventReport implements IEventReporter {
    private static final Logger log = LoggerFactory.getLogger(CombinedEventReport.class);
    private NilsReporter nilsReporter;
    private EventGraphiteReporter graphiteReporter;

    public CombinedEventReport(NilsReporter nilsReporter, EventGraphiteReporter graphiteReporter) {
        this.nilsReporter = nilsReporter;
        this.graphiteReporter = graphiteReporter;
    }

    @Override
    public void reportEvent(Object event) {
        log.debug("Report event to nils server and the graphite server");
        nilsReporter.reportEvent(event);
        graphiteReporter.reportEvent(event);
    }

    @Override
    public void reportHistogramEvent(EventHistogram eventHistogram) {
        log.debug("Report event to nils server and the graphite server");
        nilsReporter.reportHistogramEvent(eventHistogram);
        graphiteReporter.reportHistogramEvent(eventHistogram);

    }

    @Override
    public void reportMetricsEvent(EventMetrics eventMetrics) {

        log.debug("Report event to nils server and the graphite server");
        nilsReporter.reportMetricsEvent(eventMetrics);
        graphiteReporter.reportMetricsEvent(eventMetrics);
    }
}
