package com.nils;

import com.codahale.metrics.Histogram;
import com.codahale.metrics.Reservoir;
import com.codahale.metrics.SlidingWindowReservoir;
import com.codahale.metrics.Snapshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by kobi on 5/23/15.
 * A metrics manager that holds all the data for a given actor system
 */
public class MetricsManager {
    private static final Logger log = LoggerFactory.getLogger(MetricsManager.class);
    public static MetricsManager instance = new MetricsManager();
    private Map<String, AtomicInteger> atomicCounterMap;
    private Histogram histogram;

    private MetricsManager(){init();}

    private void init(){
        log.debug("create a new metrics manager");
        Reservoir reservoir = new SlidingWindowReservoir(1000);
        this.histogram = new Histogram(reservoir);
        atomicCounterMap = new HashMap<>();
    }

    public Snapshot getHistogramSnapshot(){
        log.debug("getHistogramSnapshot was called");
        return this.histogram.getSnapshot();
    }

    public int getAndIncrement(String counterName){
        log.debug("getAndIncrement was called with name counter name: {}", counterName);
        if (atomicCounterMap.containsKey(counterName)){
            return atomicCounterMap.get(counterName).getAndIncrement();
        } else {
            AtomicInteger atomicInteger = new AtomicInteger(1);
            atomicCounterMap.put(counterName,atomicInteger);
            return atomicInteger.getAndIncrement();
        }
    }

    public void updateHistogram(Long value){
        this.histogram.update(value);
    }


}
