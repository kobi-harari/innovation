package com.nils.aspect;

import akka.actor.ActorSystem;
import com.nils.akka.AkkaManager;
import com.nils.model.event.EventApplicationCreated;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by kobi.harari on 5/21/2015.
 * Aspect that will monitor the actor system
 */
@Aspect
public class ActorSystemAspect {
    private final Logger log = LoggerFactory.getLogger(ActorSystemAspect.class);
    @After("execution(* akka.actor.ActorSystem+.start(..))")
    public void afterActorSystemStarted(JoinPoint joinPoint) {
        log.debug("[ActorSystemAspect] new actor system was started");
        if(((ActorSystem)joinPoint.getTarget()).name().equals("nils-monitoring")){
            return;
        }
        AkkaManager.instance.initAkkaManager(((ActorSystem) (joinPoint.getThis())));
        EventApplicationCreated message = new EventApplicationCreated(((ActorSystem) (joinPoint.getThis())).name());
        AkkaManager.instance.sendMessage(message);

        // todo - add more data on the system
    }
}
