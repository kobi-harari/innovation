package com.nils.aspect;

import akka.actor.UntypedActor;
import com.codahale.metrics.Snapshot;
import com.nils.MetricsManager;
import com.nils.akka.AkkaManager;
import com.nils.akka.MessageGatewayActor;
import com.nils.akka.MessageSenderActor;
import com.nils.model.ActorModel;
import com.nils.model.HistogramModel;
import com.nils.model.MessageModel;
import com.nils.model.PerformanceModel;
import com.nils.model.event.*;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by kobi on 5/14/15.
 * The Aspect file that will handle aspect call
 */
@Aspect
public class ActorMonitorAspect {
    private final Logger log = LoggerFactory.getLogger(ActorMonitorAspect.class);

    @Before("execution(* akka.actor.UntypedActor+.preStart(..))")
    public void beforeActorStarted(JoinPoint joinPoint) {
        if(joinPoint.getTarget() instanceof MessageSenderActor || joinPoint.getTarget() instanceof MessageGatewayActor){return;}
        log.debug("we are about to create the following actor {}", joinPoint.getTarget().getClass().getSimpleName());
        UntypedActor untypedActor = ((UntypedActor) joinPoint.getThis());
        System.out.println(((UntypedActor) joinPoint.getThis()).context().parent().getClass());
        if (untypedActor.context().parent().getClass().toString().contains("RoutedActorRef")){
            String routerPath = untypedActor.context().parent().path().toStringWithoutAddress();
            AkkaManager.instance.sendMessage(new EventRouterDiscovered(routerPath, routerPath.substring(0, routerPath.lastIndexOf("/"))));
        }
        ActorModel model = new ActorModel(joinPoint.getTarget().getClass().toString(),
                untypedActor.getClass().getSimpleName(), untypedActor.getSelf().path().toStringWithoutAddress(),
                 -1L,System.currentTimeMillis(), untypedActor.context().parent().path().toStringWithoutAddress());
        EventActorCreated eventActorCreated = new EventActorCreated(model);
        AkkaManager.instance.sendMessage(eventActorCreated);
    }

    @Before("execution(* akka.actor.UntypedActor+.postStop(..))")
    public void beforeActorStopped(JoinPoint joinPoint) {
        if(joinPoint.getTarget() instanceof MessageSenderActor || joinPoint.getTarget() instanceof MessageGatewayActor){return;}
        UntypedActor untypedActor = ((UntypedActor) joinPoint.getThis());

        ActorModel model = new ActorModel(joinPoint.getTarget().getClass().toString(),
                untypedActor.getClass().getSimpleName(), untypedActor.getSelf().path().toStringWithoutAddress(),
                 System.currentTimeMillis(), -1L, untypedActor.context().parent().path().toStringWithoutAddress());
        EventActorTerminated eventActorTerminated = new EventActorTerminated(model);
        AkkaManager.instance.sendMessage(eventActorTerminated);
    }
    //todo - change the actor created name of the class

    @Before("execution(* akka.actor.UntypedActor+.onReceive(..))")
    public void messageReceived(JoinPoint joinPoint) {
        if(joinPoint.getTarget() instanceof MessageSenderActor || joinPoint.getTarget() instanceof MessageGatewayActor){return;}
        log.debug("onReceive message was intercepted");
        String senderPath = ((UntypedActor) joinPoint.getTarget()).getSender().path().toStringWithoutAddress();
        String thisPath = ((UntypedActor) joinPoint.getTarget()).getSelf().path().toStringWithoutAddress();

        MessageModel message = new MessageModel(senderPath, thisPath, System.currentTimeMillis(),
                System.currentTimeMillis(), joinPoint.getArgs()[0]);
        EventBaseModel messageReceived = new EventMessageReceived(message);
        AkkaManager.instance.sendMessage(messageReceived);
    }

    @Before("execution(* akka.actor.Props+.create(..)")
    public void routerCreated(JoinPoint joinPoint){
        System.out.println(joinPoint);
    }


    @Around("execution(* akka.actor.UntypedActor+.onReceive(..))")
    public Object systemMessageProcessing(ProceedingJoinPoint joinPoint) throws Throwable {
        if(joinPoint.getTarget() instanceof MessageSenderActor || joinPoint.getTarget() instanceof MessageGatewayActor){return joinPoint.proceed();}
        long start = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();//continue on the intercepted method
        long end = System.currentTimeMillis();
        MetricsManager.instance.updateHistogram((end - start));
        int processingCounter = MetricsManager.instance.getAndIncrement("totalOnReceive");
        // todo - send the histogram each second
        if (processingCounter % 20 == 0) {
            log.debug(" it is time to report the performance historgram to the server");
            HistogramModel histogramModel = createPerformanceModel(MetricsManager.instance.getHistogramSnapshot());
            EventHistogram eventHistogram = new EventHistogram(histogramModel);
            AkkaManager.instance.sendMessage(eventHistogram);
        }
        return proceed;
    }



    //
    private HistogramModel createPerformanceModel(Snapshot snapshot) {
        HistogramModel histogramModel = new HistogramModel();
        histogramModel.setMax(snapshot.getMax());
        histogramModel.setMin(snapshot.getMin());
        histogramModel.setMean(snapshot.getMean());
        histogramModel.setMedian(snapshot.getMedian());
        histogramModel.setStdDev(snapshot.getStdDev());
        histogramModel.setPercentile75(snapshot.get75thPercentile());
        histogramModel.setPercentile95(snapshot.get95thPercentile());
        histogramModel.setPercentile98(snapshot.get98thPercentile());
        histogramModel.setPercentile99(snapshot.get99thPercentile());
        histogramModel.setPercentile999(snapshot.get999thPercentile());
        return histogramModel;
    }

    @Around("execution(* akka.actor.UntypedActor+.onReceive(..))")
    public Object systemMessageProcessingEvent(ProceedingJoinPoint joinPoint) throws Throwable {
        String actorPath = ((UntypedActor) joinPoint.getTarget()).getSelf().path().toStringWithoutAddress();
        String className = joinPoint.getTarget().getClass().getSimpleName();
        long start = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();//continue on the intercepted method
        long end = System.currentTimeMillis();
        Long processingTimeMicro = (end - start);
        //todo - add the type (class) of the message
        PerformanceModel performanceModel = new PerformanceModel(actorPath, className, processingTimeMicro);
        EventMetrics eventMetrics = new EventMetrics(performanceModel);
        AkkaManager.instance.sendMessage(eventMetrics);
        return proceed;
    }

}

//        my parent senderPath - ((UntypedActor)joinPoint.getTarget()).getSelf().senderPath().parent()
//        System.out.println("name: " + joinPoint.getSignature().getName());
//        System.out.println("class name: " + joinPoint.getTarget().getClass().getSimpleName());
//        System.out.println("class reference: " + joinPoint.getTarget().getClass());
//        System.out.println("kind: " +((UntypedActor) joinPoint.getThis()).getContext().parent());
//        System.out.println("kind: " +((UntypedActor) joinPoint.getThis()).getContext().parent());
//        System.out.println("path: " +((UntypedActor) joinPoint.getThis()).getSelf().path());
//        System.out.println("sender: " +((UntypedActor) joinPoint.getThis()).getSender());

//        System.out.println("name: " + joinPoint.getSignature().getName());
//        System.out.println("class name: " + joinPoint.getTarget().getClass().getSimpleName());
//        System.out.println("class reference: " + joinPoint.getTarget().getClass());
//        System.out.println("kind: " +((UntypedActor) joinPoint.getThis()).getContext().parent());
//        System.out.println("kind: " +((UntypedActor) joinPoint.getThis()).getContext().parent());
//        System.out.println("path: " +((UntypedActor) joinPoint.getThis()).getSelf().path());
//        System.out.println("sender: " +((UntypedActor) joinPoint.getThis()).getSender());
//System.out.println("name: " + joinPoint.getSignature().getName());
//        System.out.println("class name: " + joinPoint.getTarget().getClass().getSimpleName());
//        System.out.println("class reference:  " + joinPoint.getTarget().getClass());
//        System.out.println("message: " + joinPoint.getArgs()[0]);//