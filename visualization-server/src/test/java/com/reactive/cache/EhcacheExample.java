package com.reactive.cache;

import net.sf.ehcache.*;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.event.CacheEventListener;
import net.sf.ehcache.event.CacheEventListenerAdapter;
import net.sf.ehcache.event.RegisteredEventListeners;

/**
 * Created by kobi.harari on 5/19/2015.
 */
public class EhcacheExample {
    private CacheManager cacheManager;
    public static void main(String[] args) {
        EhcacheExample example = new EhcacheExample();
        example.initCache("testCache");
        example.saveValue("testCache","key","value");
        example.readValue("testCache","key");
        example.shutdown();

        testNotifications();
    }

    private static void testNotifications() {
        CacheManager cacheManager = CacheManager.create();
        Cache cacheListenerExample = new Cache(new CacheConfiguration("testListener",10)
                .timeToLiveSeconds(1)
                .timeToIdleSeconds(1)
                .eternal(false)
                .maxEntriesLocalDisk(0)
                .diskExpiryThreadIntervalSeconds(1));
        cacheManager.addCache(cacheListenerExample);
        cacheListenerExample.getCacheEventNotificationService().registerListener(new CacheEventListenerAdapter() {
            @Override
            public void notifyElementRemoved(Ehcache cache, Element element) throws CacheException {
                System.out.println("Element was removed " + element);
            }

            @Override
            public void notifyElementPut(Ehcache cache, Element element) throws CacheException {
                System.out.println("Element was put " + element);
            }

            @Override
            public void notifyElementUpdated(Ehcache cache, Element element) throws CacheException {

            }

            @Override
            public void notifyElementExpired(Ehcache cache, Element element) {
                System.out.println("Element was expired " + element);
            }

            @Override
            public void notifyElementEvicted(Ehcache cache, Element element) {
                System.out.println("Element was evicted " + element);
            }

            @Override
            public void notifyRemoveAll(Ehcache cache) {

            }

            @Override
            public void dispose() {

            }
        });
        cacheListenerExample.put(new Element("test1","test1"));
        cacheListenerExample.put(new Element("test2","test2"));
        cacheListenerExample.remove("test1");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
        }
    }

    private void initCache(String cacheName) {
        this.cacheManager = CacheManager.create(getClass().getResource("/ehcache.xml"));
        Cache memoryOnlyCache = new Cache(cacheName, 5000, true, true, 1000, 1000);
        cacheManager.addCache(memoryOnlyCache);
    }

    private void saveValue(String cacheName, String key, String value) {
        Element element = new Element(key,value);
        this.cacheManager.getCache(cacheName).put(element);
    }

    private void readValue(String cacheName, String key) {
        Element element = this.cacheManager.getCache(cacheName).get(key);
        System.out.println(element);
    }

    private void shutdown(){
        System.out.println("Shutting down");
        this.cacheManager.shutdown();
    }

}
