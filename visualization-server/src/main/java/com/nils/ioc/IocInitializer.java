package com.nils.ioc;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

import java.util.List;

/**
 * @author tal.maayani on 4/6/2015.
 */
public class IocInitializer {
    private Injector injector;
    private List<Module> modules;

    private static IocInitializer instance = new IocInitializer();

    public static IocInitializer getInstance() {
        return instance;
    }

    public Injector getInjector() {
        if (this.injector != null) {
            return this.injector;
        } else {
            this.injector = Guice.createInjector(modules);
        }
        return this.injector;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
    }

    public static <T> T provide(Class<T> clazz) {
        return instance.getInjector().getInstance(clazz);
    }
}
