package com.nils.ioc;

import com.google.inject.Binder;
import com.google.inject.Module;
import com.nils.data.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class applicationModule implements Module {
    private static final Logger log = LoggerFactory.getLogger(applicationModule.class);

    private static boolean isInitialized = false;


    @SuppressWarnings("unchecked")
    @Override
    public void configure(Binder binder) {
        binder.bind(ApplicationService.class).asEagerSingleton();
        binder.bind(TreeService.class).asEagerSingleton();
        binder.bind(AppEventService.class).asEagerSingleton();
        binder.bind(InputEventService.class).asEagerSingleton();
        binder.bind(ActorService.class).asEagerSingleton();
    }


}
