package com.nils.model;

import jersey.repackaged.com.google.common.collect.Sets;

import java.util.Set;

/**
 * @author tal.maayani on 7/14/2015.
 */
public class GraphData {
    Set<GraphNode> nodes = Sets.newHashSet();
    Set<GraphEdge> edges = Sets.newHashSet();

    public GraphData() {
    }

    public void addNode(GraphNode graphNode) {
        nodes.add(graphNode);
    }

    public void addEdge(GraphEdge graphEdge) {
        edges.add(graphEdge);
    }

    public Set<GraphNode> getNodes() {
        return nodes;
    }



    public Set<GraphEdge> getEdges() {
        return edges;
    }


}
