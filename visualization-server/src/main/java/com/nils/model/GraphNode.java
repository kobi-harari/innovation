package com.nils.model;

import java.util.Map;

/**
 * For use with visjs
 * see http://visjs.org/docs
 *
 * @author tal.maayani on 7/14/2015.
 */
public class GraphNode {
    String instance;
    String label;
    String group;
    Long startTime;
    String id;
    private String path = null;
    private final String className;
    String shape = "ellipse";
    String title;
    Map<String,Object> scaling;


    String parentInstance;

    public GraphNode(String instance, String parentInstance) {
        this.instance = instance;
        this.parentInstance = parentInstance;
        this.id = instance;
        this.className = "";
        this.startTime = System.currentTimeMillis();
        this.shape = "star";
        this.title = "Router";
    }

    public GraphNode(String instance) {
        this.instance = instance;
        this.id = instance;
        this.className = "";
        this.startTime = System.currentTimeMillis();
        this.shape = "diamond";
        this.title = "Root";
    }

    public GraphNode(ActorModel actor) {
        this.instance = actor.getInstanceName();
        this.className = actor.getClassName();
        this.label = actor.getPath();
        if (label.lastIndexOf("/") > 0) {
            label = label.substring(label.lastIndexOf("/") + 1);
        }
        this.path = actor.getPath();
        this.group = actor.getClassName();
        this.startTime = actor.getStartTime();
        this.parentInstance = actor.getParentKey();
        this.id = actor.getPath();
        this.title = this.className + "Actor";
    }

    public String getInstance() {
        return instance;
    }

    public String getLabel() {
        return label;
    }

    public String getGroup() {
        return group;
    }

    public Long getStartTime() {
        return startTime;
    }

    public String getParentInstance() {
        return parentInstance;
    }

    public String getId() {
        return id;
    }

    public String getClassName() {
        return className;
    }

    public String getPath() {
        return path;
    }
}
