package com.nils.model;

/**
 * @author tal.maayani on 7/13/2015.
 */
public enum PerformanceMethod {
    Mean(actorAggregatedData -> actorAggregatedData.getHistogram().getSnapshot().getMean()),
    Max(actorAggregatedData -> actorAggregatedData.getHistogram().getSnapshot().getMax()),
    Min(actorAggregatedData -> actorAggregatedData.getHistogram().getSnapshot().getMin()),
    Count(actorAggregatedData -> actorAggregatedData.getHistogram().getCount())

    ;

    DataPerformanceGetter dataPerformanceGetter;

    PerformanceMethod(DataPerformanceGetter dataPerformanceGetter) {
        this.dataPerformanceGetter = dataPerformanceGetter;
    }

    public Number getPerformanceValue(ActorAggregatedData actorAggregatedData) {
        return dataPerformanceGetter.getData(actorAggregatedData);
    }

    private interface DataPerformanceGetter {
        Number getData(ActorAggregatedData actorAggregatedData);
    }
}
