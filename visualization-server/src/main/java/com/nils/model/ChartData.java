package com.nils.model;

import jersey.repackaged.com.google.common.collect.Lists;
import jersey.repackaged.com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by kobi on 7/13/15.
 */
public class ChartData implements Serializable {
    transient Map<String, Number> chartData = new HashMap<>();
    Set<String> labels;
    List<Number> data;

    public ChartData() {
    }

    public void updateData(String labelName, Number count) {
        String[] split = labelName.split("/");
        labelName = split[split.length-1] + "#" + split[split.length-2];
        if (labelName.length() > 8) {
            labelName = labelName.substring(0,7) + "..";
        }
        chartData.put(labelName, count);
    }

    public void finish() {
        labels = Sets.newLinkedHashSet();
        data = Lists.newLinkedList();
        chartData.forEach((k, v) -> {
            data.add(v);
            labels.add(k);
        });
    }

    public Set<String> getLabels() {
        labels = chartData.keySet();
        return labels;
    }

    public List<Number> getData() {
        return data;
    }

    public void setLabels(Set<String> labels) {
        this.labels = labels;
    }

    public void setData(List<Number> data) {
        this.data = data;
    }
}
