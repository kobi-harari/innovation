package com.nils.model;

import com.codahale.metrics.Histogram;
import com.codahale.metrics.Reservoir;
import com.codahale.metrics.SlidingWindowReservoir;
import com.codahale.metrics.Snapshot;
import java.io.Serializable;

/**
 * Created by kobi on 7/13/15.
 */
public class ActorAggregatedData implements Serializable {
    private String actorPath;
    private String className;
    private HistogramModel model = new HistogramModel();

    transient Histogram histogram;

    public ActorAggregatedData(String actorPath, String className) {
        this.actorPath= actorPath;
        this.className = className;
        Reservoir reservoir = new SlidingWindowReservoir(1000);
        this.histogram = new Histogram(reservoir);
    }

    public ActorAggregatedData(String className) {
        this.className = className;
        Reservoir reservoir = new SlidingWindowReservoir(1000);
        this.histogram = new Histogram(reservoir);
    }

    public String getActorPath() {
        return actorPath;
    }

    public void setActorPath(String actorPath) {
        this.actorPath = actorPath;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Histogram getHistogram() {
        return histogram;
    }

    public void setHistogram(Histogram histogram) {
        this.histogram = histogram;
    }

    public void setValue(long value) {
        this.histogram.update(value);
        updateHistogramState();

    }

    private void updateHistogramState() {
        Snapshot snapshot = histogram.getSnapshot();
        model.setMax(snapshot.getMax());
        model.setMin(snapshot.getMin());
        model.setMean(snapshot.getMean());
        model.setMedian(snapshot.getMedian());
        model.setStdDev(snapshot.getStdDev());
        model.setPercentile75(snapshot.get75thPercentile());
        model.setPercentile95(snapshot.get95thPercentile());
        model.setPercentile98(snapshot.get98thPercentile());
        model.setPercentile99(snapshot.get99thPercentile());
        model.setPercentile999(snapshot.get999thPercentile());

    }

}
