package com.nils.model;

import jersey.repackaged.com.google.common.collect.Maps;

import java.util.Map;

/**
 * @author tal.maayani on 7/14/2015.
 */
public class GraphEdge {
    String from;
    String to;

    String label;
    String title;
    String arrows = "to";
    boolean dashes = false;
    int messages = 0;

    Map<String, Object> smooth = initSmooth();

    private Map<String, Object> initSmooth() {
        Map<String, Object> map = Maps.newHashMap();
        map.put("enabled", true);
        return map;
    }

    public GraphEdge(String from, String to, int messages) {
        this(from, to);
        arrows = "middle";
        dashes = true;
        this.messages = messages;
         updateTitle();
    }

    private void updateTitle() {
        this.title = (messages + " messages sent");
    }


    public GraphEdge(String from, String to) {
        this.from = from;
        this.to = to;
        this.title = "Child";
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getLabel() {
        return label;
    }

    public String getArrows() {
        return arrows;
    }

    public boolean isDashes() {
        return dashes;
    }

    public Map<String, Object> getSmooth() {
        return smooth;
    }

    public void incrementMessages() {
        this.messages++;
        updateTitle();
    }

    public int getMessages() {
        return messages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GraphEdge graphEdge = (GraphEdge) o;

        if (from != null ? !from.equals(graphEdge.from) : graphEdge.from != null) return false;
        if (to != null ? !to.equals(graphEdge.to) : graphEdge.to != null) return false;
        return !(arrows != null ? !arrows.equals(graphEdge.arrows) : graphEdge.arrows != null);

    }

    @Override
    public int hashCode() {
        int result = from != null ? from.hashCode() : 0;
        result = 31 * result + (to != null ? to.hashCode() : 0);
        result = 31 * result + (arrows != null ? arrows.hashCode() : 0);
        return result;
    }
}
