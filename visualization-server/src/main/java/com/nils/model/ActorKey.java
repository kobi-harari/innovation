package com.nils.model;

import java.io.Serializable;

/**
 * @author tal.maayani on 7/13/2015.
 */
public class ActorKey implements Serializable {
    String appKey;
    String className;
    String instanceName;

    public ActorKey(String appKey, String className, String instanceName) {
        this.appKey = appKey;
        this.className = className;
        this.instanceName = instanceName;
    }

    public ActorKey(String appKey, String className) {
        this.appKey = appKey;
        this.className = className;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActorKey actorKey = (ActorKey) o;

        if (appKey != null ? !appKey.equals(actorKey.appKey) : actorKey.appKey != null) return false;
        if (className != null ? !className.equals(actorKey.className) : actorKey.className != null) return false;
        return !(instanceName != null ? !instanceName.equals(actorKey.instanceName) : actorKey.instanceName != null);

    }

    @Override
    public int hashCode() {
        int result = appKey != null ? appKey.hashCode() : 0;
        result = 31 * result + (className != null ? className.hashCode() : 0);
        result = 31 * result + (instanceName != null ? instanceName.hashCode() : 0);
        return result;
    }
}
