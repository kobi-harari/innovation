package com.nils.model;

import java.util.HashSet;
import java.util.Set;

/**
 * @author tal.maayani on 7/13/2015.
 */
public class TreeActor {
    public static final int DEFAULT_SIZE = 100;
    String name;
    String type;
    String className;
    Long startTime;
    Integer size;
    Set<TreeActor> children = new HashSet<>();

    public TreeActor(ActorModel actorModel) {
        this.name = actorModel.getPath();
        this.type = actorModel.getClassName();
        this.className = actorModel.getClassName();
        this.startTime = actorModel.getStartTime();
        this.size = DEFAULT_SIZE;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<TreeActor> getChildren() {
        return children;
    }

    public void setChildren(Set<TreeActor> children) {
        this.children = children;
    }

    public void addChild(ActorModel actorModel) {
        this.children.add(new TreeActor(actorModel));
    }

    public void addChild(TreeActor treeActor) {
        this.children.add(treeActor);
        calcSize();
    }

    private void calcSize() {
        final int[] size = {0};
        this.children.forEach(c -> size[0] += c.getSize());
        this.size = size[0];
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TreeActor treeActor = (TreeActor) o;

        return !(name != null ? !name.equals(treeActor.name) : treeActor.name != null);

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
