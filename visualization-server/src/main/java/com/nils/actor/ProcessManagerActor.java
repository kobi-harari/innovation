package com.nils.actor;

import akka.actor.*;
import akka.japi.Function;
import com.nils.data.InputEventService;
import com.nils.ioc.IocInitializer;
import com.nils.message.BaseMessage;
import com.nils.message.InitApplicationMessage;
import com.nils.message.ProcessMessage;
import com.nils.model.event.EventBaseModel;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.event.CacheEventListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

/**
 * @author tal.maayani on 4/6/2015.
 */
public class ProcessManagerActor extends BaseApplicationActor {
    Logger log = LoggerFactory.getLogger(ProcessManagerActor.class);


    public static final int MAX_NR_OF_RETRIES = 10;

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return new OneForOneStrategy(MAX_NR_OF_RETRIES,
                Duration.create(1, TimeUnit.MINUTES),
                new Function<Throwable, SupervisorStrategy.Directive>() {
                    @Override
                    public SupervisorStrategy.Directive apply(Throwable t) {
                        if (t instanceof ActorInitializationException) {
                            log.error("Failed to create Specific actor - ActorInitializationException:", t);
                            return SupervisorStrategy.stop();
                        } else if (t instanceof Exception) {
                            log.error("Restarting Process manager actor due to {}", t.getMessage());
                            return SupervisorStrategy.restart();
                        } else {
                            log.error("Escalating {} due to {}", this.getClass(), t.getMessage());
                            return SupervisorStrategy.escalate();
                        }
                    }
                });
    }


    @Override
    protected void handleMessage(BaseMessage baseMessage) throws Exception {
        if (baseMessage instanceof InitApplicationMessage) {
            log.info("Creating {} actor..", EventReceiverActor.class.getSimpleName());
            getContext().actorOf(Props.create(EventReceiverActor.class), EventReceiverActor.class.getSimpleName());

            log.info("Creating {} actor..",EventProcessorActor.class.getSimpleName());
            ActorRef eventProcessor = getContext().actorOf(Props.create(EventProcessorActor.class), EventProcessorActor.class.getSimpleName());

            InputEventService inputEventService = IocInitializer.provide(InputEventService.class);
            inputEventService.getInputEventCache().getCacheEventNotificationService().registerListener(new EventProcessorAdapter(getSelf(), eventProcessor));
        }
    }


    private class EventProcessorAdapter extends CacheEventListenerAdapter {
        private final ActorRef eventProcessor;
        private final ActorRef self;

        public EventProcessorAdapter(ActorRef self, ActorRef eventProcessor) {
            this.eventProcessor = eventProcessor;
            this.self = self;
        }

        @Override
        public void notifyElementPut(Ehcache cache, Element element) throws CacheException {
            eventProcessor.tell(new ProcessMessage(System.currentTimeMillis(), (EventBaseModel) element.getObjectValue()),self);
        }
    }
}
