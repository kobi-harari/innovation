package com.nils.actor;

import com.nils.data.ActorService;
import com.nils.data.ApplicationService;
import com.nils.data.GraphService;
import com.nils.data.InputEventService;
import com.nils.ioc.IocInitializer;
import com.nils.message.BaseMessage;
import com.nils.message.ProcessMessage;
import com.nils.model.ApplicationModel;
import com.nils.model.event.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;

/**
 * User: Tal M
 * Date: 5/23/15
 */
public class EventProcessorActor extends BaseApplicationActor {
    private final InputEventService inputEventService;
    private final ApplicationService applicationService;
    private final GraphService graphService;
    private final ActorService actorService;

    Logger log = LoggerFactory.getLogger(EventProcessorActor.class);

    public EventProcessorActor() {
        inputEventService = IocInitializer.provide(InputEventService.class);
        applicationService = IocInitializer.provide(ApplicationService.class);
        graphService = IocInitializer.provide(GraphService.class);
        actorService = IocInitializer.provide(ActorService.class);
    }

    @Override
    protected void handleMessage(BaseMessage baseMessage) throws Exception {
        if (baseMessage instanceof ProcessMessage) {
            ProcessMessage processMessage = (ProcessMessage) baseMessage;
            String appKey = processMessage.getEventBaseModel().getAppKey();
            log.debug("Processing events for app {}..", appKey);
            inputEventService.getAppEvents(appKey).stream()
                    .sorted(Comparator.comparing(EventBaseModel::getSequence)).forEach(event -> {
                if (handleEvent(event)) {
                    inputEventService.removeEvent(event);
                }
            });
        }
    }

    /**
     * Handle event according to event type. Calling dedicated actor for handling event type and populating metrics
     *
     * @param event agent event
     */
    private boolean handleEvent(EventBaseModel event) {
        log.debug("Handling event {}..", event);
        if (event instanceof EventApplicationCreated) {
            EventApplicationCreated e = (EventApplicationCreated) event;
            ApplicationModel applicationModel = new ApplicationModel(e.getName(), e.getAppKey());
            applicationModel.setStartSequence(e.getSequence());
            applicationModel.setSequence(e.getSequence());
            applicationService.addApplication(applicationModel);
            return true;
        }
        if (event instanceof EventActorCreated) {
            EventActorCreated actorCreated = (EventActorCreated) event;
            graphService.updateGraph(event.getAppKey(), actorCreated);
            return true;
        }
        if (event instanceof EventRouterDiscovered) {
            graphService.updateGraph(event.getAppKey(), (EventRouterDiscovered) event);
            return true;
        }
        if (event instanceof EventMessageReceived) {
            graphService.updateGraphEdge(event.getAppKey(), (EventMessageReceived) event);
        }
        if (event instanceof EventActorTerminated) {
            graphService.updateGraph(event.getAppKey(), (EventActorTerminated) event);
            return true;
        }
        if (event instanceof EventMetrics) {
            EventMetrics eventMetrics = (EventMetrics) event;
            actorService.addMetrics(eventMetrics);
            return true;
        }


        getContext().actorSelection("/user/" + EventStreamActor.class.getSimpleName()).tell(event, getSelf());
        // TODO: handle all event types
        log.debug("Event {} was not handled", event);
        return false;
    }
}
