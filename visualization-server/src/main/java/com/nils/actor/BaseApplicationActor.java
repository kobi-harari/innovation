package com.nils.actor;

import akka.actor.UntypedActor;
import com.nils.message.BaseMessage;

/**
 * @author tal.maayani on 4/8/2015.
 */
public abstract class BaseApplicationActor extends UntypedActor {

    public static final String USER_SELECTION_PREFIX = "/user/";


    @Override
    public void onReceive(Object o) throws Exception {
        handleMessage((BaseMessage) o);
    }


    protected abstract void handleMessage(BaseMessage baseMessage) throws Exception;

}
