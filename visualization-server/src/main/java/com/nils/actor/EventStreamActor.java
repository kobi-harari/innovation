package com.nils.actor;

import akka.actor.UntypedActor;
import com.google.gson.Gson;
import com.nils.model.event.EventBaseModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.RemoteEndpoint;

/**
 * @author tal.maayani on 7/13/2015.
 */
public class EventStreamActor extends UntypedActor {
    Logger log = LoggerFactory.getLogger(EventStreamActor.class);

    private final RemoteEndpoint.Async remote;
    private static Gson gson = new Gson();


    public EventStreamActor(RemoteEndpoint.Async remote) {
        this.remote = remote;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof EventBaseModel) {
            EventBaseModel eventBaseModel = (EventBaseModel) message;
            log.debug("Sending {} to web socket client", eventBaseModel);
            remote.sendObject(gson.toJson(eventBaseModel));
        }
    }
}
