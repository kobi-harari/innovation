package com.nils.actor;

import com.nils.data.InputEventService;
import com.nils.ioc.IocInitializer;
import com.nils.message.AgentMessage;
import com.nils.message.BaseMessage;
import com.nils.data.AppEventService;
import com.nils.model.event.EventBaseModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author tal.maayani on 5/21/2015.
 */
public class EventReceiverActor extends BaseApplicationActor {
    private final AppEventService eventService;
    private final InputEventService inputEventService;
    Logger log = LoggerFactory.getLogger(EventReceiverActor.class);

    public EventReceiverActor() {
        log.debug("Ctor {} ..", this.getClass());
        this.eventService = IocInitializer.provide(AppEventService.class);
        this.inputEventService = IocInitializer.provide(InputEventService.class);
    }

    /**
     * Save input event for handling async
     */
    @Override
    protected void handleMessage(BaseMessage baseMessage) throws Exception {
        AgentMessage event = (AgentMessage) baseMessage;
        log.debug("Got Event {}, saving input event", event);
        EventBaseModel agentEvent = event.getEventBaseModel();
        inputEventService.addEvent(agentEvent);
    }
}
