package com.nils.websocket;


import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import com.nils.actor.EventReceiverActor;
import com.nils.actor.EventStreamActor;
import com.nils.application.ReactiveServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.util.concurrent.Future;

/**
 * @author tal.maayani on 5/18/2015.
 */
@ServerEndpoint("/events")
public class EventServerSocket {
    private static final Logger log = LoggerFactory.getLogger(EventServerSocket.class);

    private RemoteEndpoint.Async remote;
    private ActorRef eventStreamActor = null;


    @OnOpen
    public void onConnect(Session session) {
        System.out.println("WebSocket Opened");
        this.remote = session.getAsyncRemote();
        log.info("Creating {} actor..", EventStreamActor.class.getSimpleName());
        if (eventStreamActor != null) {
            log.info("Kill event stream actor and re-create new one");
            this.eventStreamActor.tell(PoisonPill.getInstance(), this.eventStreamActor);
        }
        log.debug("Creating new event stream actor with new remote");
        this.eventStreamActor = ReactiveServer.system.actorOf(Props.create(EventStreamActor.class, remote), EventStreamActor.class.getSimpleName());
    }

    @OnMessage
    public void onMessage(String message) {
        System.out.println("Message from Client: " + message);
        Future<Void> voidFuture = remote.sendText("Hi Client");
        System.out.println("Is done " + voidFuture.isDone());
    }

    @OnClose
    public void onClose(CloseReason reason) {
        System.out.println("WebSocket Closed. Code:" + reason);
    }

    @OnError
    public void onWebSocketError(Throwable cause) {
        cause.printStackTrace(System.err);
    }


}
