package com.nils.application;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.google.inject.Module;
import com.nils.actor.ProcessManagerActor;
import com.nils.ioc.IocInitializer;
import com.nils.ioc.applicationModule;
import com.nils.message.InitApplicationMessage;
import com.nils.websocket.EventServerSocket;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.websocket.jsr356.server.ServerContainer;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import javax.servlet.ServletException;
import javax.websocket.DeploymentException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * @author kobi.harari on 5/14/2015.
 */
public class ReactiveServer {
    private static final Logger log = LoggerFactory.getLogger(ReactiveServer.class);
    public static final int PORT = 9898;
    private static final String CLUSTER_NAME = "ReactiveServer";
    public static ActorSystem system;


    public static void main(String[] args) {
        log.info("Starting server...");

        initInjector();

        initActorSystem(CLUSTER_NAME);
        startActorSystem();

        initServer();

        log.info("Server started");

    }

    private static void initActorSystem(String name) {
        log.info("-----------------------------------");
        log.info("-----Init Actor System Client------");
        log.info("-----------------------------------");
        system = ActorSystem.create(name);
        log.info("ActorSystem '{}' was created successfully", name);
    }

    private static void startActorSystem() {
        log.info("-----------------------------------");
        log.info("--- Start Actor System ----");
        log.info("-----------------------------------");

        ActorRef mainActor = system.actorOf(Props.create(ProcessManagerActor.class), ProcessManagerActor.class.getSimpleName());
        mainActor.tell(new InitApplicationMessage(System.currentTimeMillis()), mainActor);
    }

    /**
     * Start server
     */
    private static void initServer() {
        Server server;
        log.debug("Starting the server on port " + PORT);
        server = new Server(PORT);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        ServletHolder servletHolder = new ServletHolder(new ServletContainer(new ReactiveJerseyConfig()));
        context.addServlet(servletHolder, "/*");
        server.setHandler(context);
        context.addFilter(CustomResponseFilter.class.getName(), "/*", EnumSet.of(DispatcherType.ERROR, DispatcherType.FORWARD, DispatcherType.INCLUDE, DispatcherType.REQUEST));
        try {
            ServerContainer wscontainer = WebSocketServerContainerInitializer.configureContext(context);
            wscontainer.addEndpoint(EventServerSocket.class);
        } catch (DeploymentException | ServletException e) {
            log.error("Error init server", e);
        }

        server.setDumpAfterStart(true);
        server.setDumpBeforeStop(true);
        try {
            server.start();
        } catch (Exception e) {
            log.error("could not start the Jetty server! " + e.getMessage(), e);
        }
    }


    private static void initInjector() {
        log.info("-----------------------------------");
        log.info("---------- Init Injector ----------");
        log.info("-----------------------------------");

        List<Module> moduleList = new ArrayList<>();
        moduleList.add(new applicationModule());
        IocInitializer.getInstance().setModules(moduleList);
        log.info("-----------------------------------");
        log.info("----- Init Injector completed -----");
        log.info("-----------------------------------");


    }
}
