package com.nils.application;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author kobi.harari on 5/14/2015.
 */
public class ReactiveJerseyConfig extends ResourceConfig {

    private static final Logger log = LoggerFactory.getLogger(ReactiveJerseyConfig.class);
    public ReactiveJerseyConfig() {
        try {
            packages("com.nils.controller");
        } catch (Exception e) {
            log.error("Could not init the packages",e);
        }
        property(ServerProperties.TRACING, "ALL");
        property("com.sun.jersey.api.json.POJOMappingFeature","true");
        log.debug("***** Jersey Application started *****");
    }
}
