package com.nils.application;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CustomResponseFilter implements ContainerResponseFilter, Filter{

	@Override
	public void filter(ContainerRequestContext requestContext,
			ContainerResponseContext responseContext) throws IOException {

		responseContext.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		responseContext.getHeaders().putSingle("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
		responseContext.getHeaders().putSingle("Access-Control-Allow-Headers", "Content-Type, accept, Authorization");
		responseContext.getHeaders().putSingle("Access-Control-Expose-Headers", "Authorization");
		List<String> reqHead = requestContext.getHeaders().get("Access-Control-Request-Headers");

		if (null != reqHead) {
			responseContext.getHeaders().put("Access-Control-Allow-Headers",
					new ArrayList<Object>(reqHead));
		}
	}

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.addHeader("Access-Control-Allow-Origin", "*");
        httpResponse.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        httpResponse.addHeader("Access-Control-Allow-Headers", "Content-Type, accept, Authorization");
        httpResponse.addHeader("Access-Control-Expose-Headers", "Authorization");
        Collection<String> reqHead = httpResponse.getHeaders("Access-Control-Request-Headers");

        if (null != reqHead) {
            for (String r : reqHead) {
                httpResponse.addHeader("Access-Control-Allow-Headers",r);
            }
        }
        chain.doFilter(request,response);
    }

    @Override
    public void destroy() {

    }
}
