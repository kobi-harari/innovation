package com.nils.controller;

import com.google.gson.Gson;
import com.nils.data.ActorService;
import com.nils.data.ApplicationService;
import com.nils.data.GraphService;
import com.nils.data.TreeService;
import com.nils.ioc.IocInitializer;
import com.nils.model.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * User: Tal M
 * Date: 6/2/15
 */
@Path("/applications")
public class ApplicationController {

    private static Gson gson = new Gson();

    @GET
    public String getApplications() {
        List<ApplicationModel> applications = IocInitializer.provide(ApplicationService.class).getApplications();
        return gson.toJson(applications);
    }

    @GET
    @Path("/{appKey}/graph")
    public String getApplicationGraph(@PathParam("appKey") String appKey) {
        GraphService graphService = IocInitializer.provide(GraphService.class);
        GraphData graph = graphService.getGraph(appKey);
        return gson.toJson(graph);
    }

    @GET
    @Path("/performance/{appKey}/{actorClass}/{method}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getActorTypeMetrics(@PathParam("appKey") String appKey, @PathParam("actorClass") String actorClass, @PathParam("method") PerformanceMethod method) {
        ActorService actorService = IocInitializer.provide(ActorService.class);
        ChartData chartData = actorService.getActorClassPerformanceData(appKey, actorClass, method);
        return gson.toJson(chartData);
    }

    @GET
    @Path("/{appKey}/{actorClass}/{instanceName}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getActorInstancesMetrics(@PathParam("appKey") String appKey, @PathParam("actorClass") String actorClass, @PathParam("instanceName") String instance) {
        ActorService actorService = IocInitializer.provide(ActorService.class);
        ActorAggregatedData actorData = actorService.getActorInstanceData(appKey, actorClass, instance);
        return gson.toJson(actorData);
    }

    @GET
    @Path("/{appKey}/{actorClass}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getActorParameter(@PathParam("appKey") String appKey, @PathParam("actorClass") String actorClass) {
        ActorService actorService = IocInitializer.provide(ActorService.class);
        ActorAggregatedData actorData = actorService.getActorData(appKey, actorClass);
        return gson.toJson(actorData);
    }
}
