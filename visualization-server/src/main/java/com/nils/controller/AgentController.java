package com.nils.controller;

import akka.actor.ActorSelection;
import akka.pattern.Patterns;
import com.google.gson.Gson;
import com.nils.application.ReactiveServer;
import com.nils.message.AgentMessage;
import com.nils.model.event.EventBaseModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

/**
 * Cresated by kobi.harari on 5/14/2015.
 */
@Path("/")
public class AgentController {
    private static final Logger log = LoggerFactory.getLogger(AgentController.class);
    public static final long TIMEOUT_MILLIS = 10l;
    public static final String EVENT_RECEIVER_ACTOR_SELECTION = "/user/ProcessManagerActor/EventReceiverActor";

    private static Gson gson = new Gson();

    @GET
    @Path("healthCheck")
    public String getStatus() {
        log.debug("get status was called");
        return "{ \"status\": \"OK\" }";
    }

    @POST
    @Path("event")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addEvent(final EventBaseModel event) {
        log.debug("got event: {}", event);
        ActorSelection actorSelection = ReactiveServer.system.actorSelection(EVENT_RECEIVER_ACTOR_SELECTION);
        Patterns.ask(actorSelection,
                new AgentMessage(System.currentTimeMillis(), event),
                TIMEOUT_MILLIS);
    }
}

