package com.nils.data;

import com.nils.ioc.IocInitializer;
import com.nils.model.*;
import com.nils.model.event.EventActorCreated;
import com.nils.model.event.EventActorTerminated;
import com.nils.model.event.EventMessageReceived;
import com.nils.model.event.EventRouterDiscovered;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author tal.maayani on 5/20/2015.
 */
public class GraphService extends BaseDataService {
    public static final String USER = "/user";
    Logger log = LoggerFactory.getLogger(GraphService.class);

    public static final String ROOT_KEY = "root";

    public GraphService() {
        super();
    }

    /**
     * Build graph out of actor model
     */
    public GraphData getGraph(String appKey) {
        Cache nodesCache = getNodeCache(appKey);
        Cache edgesCache = getEdgesCache(appKey);
        if (nodesCache.getSize() == 0) {
            return new GraphData();
        }
        return buildGraph(nodesCache, edgesCache);
    }

    private GraphData buildGraph(Cache nodesCache, Cache edgesCache) {
        GraphData graphData = new GraphData();
        nodesCache.getKeys().forEach(key -> {
                    Element element = nodesCache.get(key);
                    if (!element.isExpired()) {
                        GraphNode node = (GraphNode) element.getObjectValue();
                        graphData.addNode(node);
                        if (node.getParentInstance() != null) {
                            graphData.addEdge(new GraphEdge(node.getParentInstance(), node.getId()));
                        }
                    } else {
                        nodesCache.remove(key);
                    }
                }
        );
        edgesCache.getKeys().forEach(key -> {
            Element element = edgesCache.get(key);
            if (!element.isExpired()) {
                graphData.addEdge((GraphEdge) element.getObjectValue());
            } else {
                edgesCache.remove(key);
            }
        });
        return graphData;
    }


    /**
     * Update graph on actor created
     */
    public void updateGraph(String appKey, EventActorCreated actorCreated) {
        Cache cache = getNodeCache(appKey);
        ActorModel actor = actorCreated.getActor();
        actor.setParentKey(getFormattedPath(actor.getParentKey()));
        if (ROOT_KEY.equals(actor.getParentKey())) {
            if (!cache.isKeyInCache(ROOT_KEY)) {
                log.debug("Add root key");
                cache.put(new Element(ROOT_KEY, new GraphNode(ROOT_KEY)));
            }
        }
        if (actor.getPath().startsWith(USER)) {
            actor.setPath(getFormattedPath(actor.getPath()));
        }
        cache.put(new Element(actor.getPath(), new GraphNode(actor)));
    }

    public void updateGraph(String appKey, EventRouterDiscovered eventRouterDiscoved) {
        Cache cache = getNodeCache(appKey);
        String routerPath = getFormattedPath(eventRouterDiscoved.getRouterPath());
        if (!cache.isKeyInCache(routerPath)) {
            cache.put(new Element(routerPath, new GraphNode(routerPath, getFormattedPath(eventRouterDiscoved.getParentPath()))));
        }
    }

    private String getFormattedPath(String actorPath) {
        if (ROOT_KEY.equals(actorPath)) return ROOT_KEY;
        int i = actorPath.indexOf(USER);
        if (i > -1) {
            String path = actorPath.substring(i + USER.length());
            if (path.length() <= 1) return ROOT_KEY;
            return path;
        } else {
            return actorPath;
        }
    }

    /**
     * Update tree on actor removed
     */
    public void updateGraph(String appKey, EventActorTerminated actorTerminated) {
        Cache cache = getNodeCache(appKey);
        ActorModel actor = actorTerminated.getActor();
        cache.remove(getFormattedPath(actor.getPath()));
    }

    private Cache getNodeCache(String appKey) {
        String cacheName = buildCacheName(CacheType.Nodes, appKey);
        if (!cacheManager.cacheExists(cacheName)) {
            IocInitializer.provide(ApplicationService.class).addNodesCache(cacheName);
        }
        return cacheManager.getCache(cacheName);
    }

    private Cache getEdgesCache(String appKey) {
        String cacheName = buildCacheName(CacheType.Edges, appKey);
        if (!cacheManager.cacheExists(cacheName)) {
            IocInitializer.provide(ApplicationService.class).addEdgesCache(cacheName);
        }
        return cacheManager.getCache(cacheName);
    }

    public void updateGraphEdge(String appKey, EventMessageReceived event) {
        Cache cache = getEdgesCache(appKey);
        MessageModel message = event.getMessage();
        String key = buildNodeKey(message);
        GraphEdge graphEdge;
        if (!cache.isKeyInCache(key)) {
            graphEdge = new GraphEdge(getFormattedPath(message.getSenderActor()), getFormattedPath(message.getReceiverActor()), 0);
        } else {
            graphEdge = (GraphEdge) cache.get(key).getObjectValue();
        }
        graphEdge.incrementMessages();
        cache.put(new Element(key, graphEdge));
    }

    private String buildNodeKey(MessageModel message) {
        return String.format("%s->%s", getFormattedPath(message.getSenderActor()), getFormattedPath(message.getReceiverActor()));
    }
}
