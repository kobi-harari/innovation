package com.nils.data;

import com.nils.model.event.EventBaseModel;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import net.sf.ehcache.search.Attribute;

import java.util.List;
import java.util.stream.Collectors;

/**
 * User: Tal M
 * Date: 5/23/15
 */
public class InputEventService extends BaseDataService {

    public String addEvent(EventBaseModel eventBaseModel) {
        String key = buildElementKey(eventBaseModel);
        getInputEventCache().put(new Element(key, eventBaseModel));
        return key;
    }

    public boolean removeEvent(EventBaseModel eventBaseModel) {
        return getInputEventCache().remove(buildElementKey(eventBaseModel));
    }

    public List<EventBaseModel> getAppEvents(String appKey) {
        Cache inputEventCache = getInputEventCache();
        Attribute<String> app = inputEventCache.getSearchAttribute("app");
        return inputEventCache.createQuery().addCriteria(app.eq(appKey)).includeValues().execute().all()
                .stream().map(r -> (EventBaseModel) r.getValue()).collect(Collectors.toList());
    }

    public Cache getInputEventCache() {
        return cacheManager.getCache(buildCacheName(CacheType.InputEvents));
    }


}
