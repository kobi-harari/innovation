package com.nils.data;

import com.nils.ioc.IocInitializer;
import com.nils.model.ActorModel;
import com.nils.model.TreeActor;
import com.nils.model.event.EventActorCreated;
import com.nils.model.event.EventActorTerminated;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author tal.maayani on 5/20/2015.
 */
public class TreeService extends BaseDataService {
    public static final String USER = "/user";
    Logger log = LoggerFactory.getLogger(TreeService.class);

    public static final String ROOT_KEY = "root";

    public TreeService() {
        super();
    }

    /**
     * Build tree out of actor model
     */
    public TreeActor getTree(String appKey) {
        Cache cache = getCache(appKey);
        if (!cache.isKeyInCache(ROOT_KEY)) {
            // no tree to build
            return null;
        }
        ActorModel rootActorModel = (ActorModel) cache.get(ROOT_KEY).getObjectValue();

        return buildTree(rootActorModel, cache);
    }

    private TreeActor buildTree(ActorModel actorModel, Cache cache) {
        TreeActor treeActor = new TreeActor(actorModel);
        actorModel.getChildren().forEach(child -> {
                    Element element = cache.get(child);
                    if (element != null) {
                        ActorModel childActor = (ActorModel) element.getObjectValue();
                        treeActor.addChild(buildTree(childActor, cache));
                    }
                }
        );
        return treeActor;
    }

    /**
     * Update tree on actor created
     */
    public void updateTree(String appKey, EventActorCreated actorCreated) {
        ActorModel actor = actorCreated.getActor();
        Cache cache = getCache(appKey);
        Element existing = cache.get(getPath(actor.getPath()));
        if (existing != null) {
            ActorModel existingActor = (ActorModel) existing.getObjectValue();
            existingActor.setChildren(existingActor.getChildren());
            cache.put(existing);
        } else {
            cache.put(new Element(getPath(actor.getPath()), actor));
        }
        if (actor.getParentKey() == null || actor.getParentKey().endsWith(USER)) {
            if (!cache.isKeyInCache(ROOT_KEY)) {
                cache.put(new Element(ROOT_KEY, new ActorModel(ROOT_KEY)));
            }
            actor.setParentKey(ROOT_KEY);
        } else if (!cache.isKeyInCache(getPath(actor.getParentKey()))) {
            log.debug("Add parent key {} to cache", getPath(actor.getParentKey()));
            ActorModel parent = new ActorModel(getPath(actor.getParentKey()));
            cache.put(new Element(getPath(actor.getParentKey()), parent));
        }

        Element parent = cache.get(getPath(actor.getParentKey()));
        ActorModel parentActor = (ActorModel) parent.getObjectValue();
        parentActor.addChildren(getPath(actor.getPath()));
        cache.put(parent);
    }

    private String getPath(String actorPath) {
        String path = actorPath.substring(actorPath.indexOf(USER) + USER.length());
        if (path.length() <= 1) return ROOT_KEY;
        return path;
    }

    /**
     * Update tree on actor removed
     */
    public void updateTree(String appKey, EventActorTerminated actorTerminated) {
        Cache cache = getCache(appKey);
        ActorModel actor = actorTerminated.getActor();
        Element parentElement = cache.get(actor.getParentKey());
        if (parentElement != null) {
            ActorModel parentActor = (ActorModel) parentElement.getObjectValue();
            parentActor.removeChildren(actor.getPath());
            cache.put(parentElement);
        }
        cache.remove(actor.getPath());
    }

    private Cache getCache(String appKey) {
        String cacheName = buildCacheName(CacheType.Tree, appKey);
        if (!cacheManager.cacheExists(cacheName)) {
            IocInitializer.provide(ApplicationService.class).addNodesCache(cacheName);
        }
        return cacheManager.getCache(cacheName);
    }
}
