package com.nils.data;

import com.nils.ioc.IocInitializer;
import com.nils.model.ActorAggregatedData;
import com.nils.model.ActorKey;
import com.nils.model.ChartData;
import com.nils.model.PerformanceMethod;
import com.nils.model.event.EventMetrics;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import net.sf.ehcache.search.Attribute;
import net.sf.ehcache.search.Query;
import net.sf.ehcache.search.Results;

/**
 * Created by kobi on 7/13/15.
 * service that will provide data on actors
 */
public class ActorService extends BaseDataService {

    public ActorService() {
        super();
    }

    public ActorAggregatedData getActorInstanceData(String appKey, String actorClassName, String instanceName) {
        Cache cache = getCache(CacheType.Actors, appKey);
        if (!instanceName.startsWith("/user")) instanceName = "/user"+instanceName;
        Element element = cache.get(createInstanceKey(appKey, actorClassName, instanceName));
        if (element == null) {
            return new ActorAggregatedData(actorClassName);
        }
        return (ActorAggregatedData) element.getObjectValue();
    }

    public ActorAggregatedData getActorData(String appKey, String className) {
        Cache cache = getCache(CacheType.Actors, appKey);
        Element element = cache.get(createInstanceKey(appKey, className, ""));
        if (element == null) {
            return new ActorAggregatedData(className);
        }
        return (ActorAggregatedData) element.getObjectValue();
    }

    public ChartData getActorClassPerformanceData(String appKey, String className, PerformanceMethod performanceMethod) {
        ChartData chartData = new ChartData();

        Cache cache = getCache(CacheType.Actors, appKey);

        Attribute<Object> appKeyAttr = cache.getSearchAttribute("appKey");
        Attribute<Object> classNameAttr = cache.getSearchAttribute("className");

        Query query = cache.createQuery();
        query.includeKeys().includeValues();

        Results results = query.addCriteria(appKeyAttr.eq(appKey).and(classNameAttr.eq(className))).execute();
        results.all().forEach(result -> {
            ActorAggregatedData actorAggData = (ActorAggregatedData) result.getValue();
            chartData.updateData(actorAggData.getActorPath(), performanceMethod.getPerformanceValue(actorAggData));
        });
        chartData.finish();

        return chartData;
    }


    public void addMetrics(EventMetrics eventMetrics) {
        addInstanceProcessingTime(eventMetrics);
        addActorTypeMetrics(eventMetrics);
    }

    public void addActorTypeMetrics(EventMetrics eventMetrics) {
        Cache cache = getCache(CacheType.Actors, eventMetrics.getAppKey());
        Element element = cache.get(createInstanceKey(eventMetrics.getAppKey(), eventMetrics.getPerformanceMode().getClassName(), ""));
        ActorAggregatedData data;
        if (element != null) {
            data = (ActorAggregatedData) element.getObjectValue();
            data.setValue(eventMetrics.getValue());
        } else {
            data = new ActorAggregatedData(eventMetrics.getPerformanceMode().getActorPath(), eventMetrics.getPerformanceMode().getClassName());
            data.setValue(eventMetrics.getValue());
        }
        cache.put(new Element(createInstanceKey(eventMetrics.getAppKey(), eventMetrics.getPerformanceMode().getClassName(), ""), data));
    }

    public void addInstanceProcessingTime(EventMetrics eventMetrics) {
        Cache cache = getCache(CacheType.Actors, eventMetrics.getAppKey());
        ActorKey key = createInstanceKey(eventMetrics.getAppKey(), eventMetrics.getPerformanceMode().getClassName(), eventMetrics.getPerformanceMode().getActorPath());
        Element existingElement = cache.get(key);
        ActorAggregatedData data;
        if (existingElement != null) {
            data = (ActorAggregatedData) existingElement.getObjectValue();
        } else {
            data = new ActorAggregatedData(eventMetrics.getPerformanceMode().getActorPath(), eventMetrics.getPerformanceMode().getClassName());
        }
        data.setValue(eventMetrics.getValue());
        Element element = new Element(key, data);
        cache.put(element);
    }

    private ActorKey createInstanceKey(String appKey, String className, String path) {
        return new ActorKey(appKey, className, path);
    }

    protected Cache getCache(CacheType cacheType, String cacheKey) {
        String cacheName = buildCacheName(cacheType, cacheKey);
        if (!cacheManager.cacheExists(cacheName)) {
            IocInitializer.provide(ApplicationService.class).addActorCache(cacheName);
        }
        return cacheManager.getCache(cacheName);
    }
}
