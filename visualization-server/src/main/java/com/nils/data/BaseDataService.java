package com.nils.data;

import com.nils.model.event.EventBaseModel;
import net.sf.ehcache.CacheManager;

import java.io.InputStream;

/**
 * @author tal.maayani on 5/20/2015.
 */
public class BaseDataService {
    protected static CacheManager cacheManager = null;

    public BaseDataService() {
        init();
    }

    private void init() {
        if (cacheManager == null) {
            InputStream resourceAsStream = getClass().getResourceAsStream("/ehcache.xml");
            cacheManager = CacheManager.create(resourceAsStream);
        }
    }

    protected String buildCacheName(CacheType cacheType, String cacheKey) {
        return String.format("%s#%s", cacheType, cacheKey);
    }

    protected String buildCacheName(CacheType cacheType) {
        return cacheType.toString();
    }

    protected String buildElementKey(EventBaseModel eventBaseModel) {
        return String.format("%s#%s#%s", eventBaseModel.getAppKey(), eventBaseModel.getEventDate(), eventBaseModel.getSequence());
    }


}
