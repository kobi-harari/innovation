package com.nils.data;

import com.nils.model.ApplicationModel;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.SearchAttribute;
import net.sf.ehcache.config.Searchable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author tal.maayani on 5/20/2015.
 */
public class ApplicationService extends BaseDataService {
    Logger log = LoggerFactory.getLogger(ApplicationService.class);

    static CacheConfiguration graphCacheConfiguration = initGraphCacheConfiguration();
    static CacheConfiguration eventsCacheConfiguration = initEventsCacheConfiguration();
    static CacheConfiguration actorCacheConfiguration = initActorCacheConfiguration();

    private static CacheConfiguration initEventsCacheConfiguration() {
        CacheConfiguration eventsCache = new CacheConfiguration();
        eventsCache.eternal(false);
        eventsCache.setMaxEntriesLocalDisk(10000000);
        eventsCache.maxEntriesLocalHeap(1000);
        eventsCache.timeToLiveSeconds(600);
        return eventsCache;
    }

    private static CacheConfiguration initGraphCacheConfiguration() {
        CacheConfiguration graphCacheConfig = new CacheConfiguration();
        graphCacheConfig.eternal(true);
        graphCacheConfig.setMaxEntriesLocalDisk(0);
        graphCacheConfig.maxEntriesLocalHeap(1000);
        return graphCacheConfig;
    }

    private static CacheConfiguration initActorCacheConfiguration() {
        CacheConfiguration cacheConfig = new CacheConfiguration();
        cacheConfig.eternal(true);
        cacheConfig.setMaxEntriesLocalDisk(0);
        cacheConfig.maxEntriesLocalHeap(1000);
        Searchable searchable = new Searchable();
        SearchAttribute searchAttributeAppKey = new SearchAttribute();
        searchAttributeAppKey.setName("appKey");
        searchAttributeAppKey.setExpression("key.getAppKey()");
        SearchAttribute searchAttributeClassName = new SearchAttribute();
        searchAttributeClassName.setName("className");
        searchAttributeClassName.setExpression("key.getClassName()");
        searchable.addSearchAttribute(searchAttributeAppKey);
        searchable.addSearchAttribute(searchAttributeClassName);
        cacheConfig.searchable(searchable);
        return cacheConfig;
    }

    public ApplicationService() {
        super();
    }

    public List<ApplicationModel> getApplications() {
        Cache applicationCache = getApplicationCache();
        List keys = applicationCache.getKeys();
        return applicationCache.getAll(keys).values().stream().map(e -> (ApplicationModel) e.getObjectValue()).collect(Collectors.toList());
    }


    public void addApplication(ApplicationModel applicationModel) {
        Cache applicationCache = getApplicationCache();
        String appTreeCacheKey = buildCacheName(CacheType.Tree, applicationModel.getKey());
        if (!applicationCache.isKeyInCache(appTreeCacheKey)) {
            // application does not exists - create app cache
            addNodesCache(appTreeCacheKey);
            String eventsCacheName = buildCacheName(CacheType.Events, applicationModel.getKey());
            addEventCache(eventsCacheName);
            updateApplication(applicationModel);
        } else {
            // do noting since application already exists
            log.warn("Use existing application cache");
        }
    }

    public void addEventCache(String eventsCacheName) {
        if (!cacheManager.cacheExists(eventsCacheName))
            cacheManager.addCache(new Cache(eventsCacheConfiguration.name(eventsCacheName)));
    }

    public void addNodesCache(String cacheName) {
        if (!cacheManager.cacheExists(cacheName))
            cacheManager.addCache(new Cache(graphCacheConfiguration.name(cacheName)));
    }

    public void addEdgesCache(String cacheName) {
        if (!cacheManager.cacheExists(cacheName))
            cacheManager.addCache(new Cache(graphCacheConfiguration.name(cacheName)));
    }

    public void addActorCache(String cacheName) {
        if (!cacheManager.cacheExists(cacheName))
            cacheManager.addCache(new Cache(actorCacheConfiguration.name(cacheName)));
    }

    public void updateApplication(ApplicationModel applicationModel) {
        Cache applicationCache = getApplicationCache();
        applicationCache.put(buildElement(applicationModel));
    }

    public void removeApplication(UUID key) {
        getApplicationCache().remove(key);
    }

    private Cache getApplicationCache() {
        return cacheManager.getCache(buildCacheName(CacheType.Applications));
    }

    private Element buildElement(ApplicationModel applicationModel) {
        return new Element(applicationModel.getKey(), applicationModel);
    }


}
