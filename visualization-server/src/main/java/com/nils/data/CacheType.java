package com.nils.data;

/**
 * @author tal.maayani on 5/21/2015.
 */
public enum CacheType {
    Applications
    ,InputEvents
    , Tree
    , Nodes
    , Edges
    ,Events
    ,Actors
}
