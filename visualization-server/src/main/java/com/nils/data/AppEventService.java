package com.nils.data;

import com.nils.model.event.EventBaseModel;
import net.sf.ehcache.Cache;
import net.sf.ehcache.Element;

/**
 * @author tal.maayani on 5/20/2015.
 */
public class AppEventService extends BaseDataService {


    public AppEventService() {
        super();
    }

    public void updateEvent(EventBaseModel eventBaseModel) {
        getEventCache(eventBaseModel.getAppKey()).put(buildEventElement(eventBaseModel));
    }

    private Element buildEventElement(EventBaseModel eventBaseModel) {
        return new Element(eventBaseModel.getSequence(), eventBaseModel);
    }

    private Cache getEventCache(String appKey) {
        return cacheManager.getCache(buildCacheName(CacheType.Events, appKey));
    }
}
