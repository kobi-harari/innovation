package com.nils.message;

import com.nils.model.event.EventBaseModel;

/**
 * User: Tal M
 * Date: 5/23/15
 */
public class ProcessMessage extends BaseMessage {
    EventBaseModel eventBaseModel;

    public ProcessMessage(long seed, EventBaseModel eventBaseModel) {
        super(seed);
        this.eventBaseModel = eventBaseModel;
    }

    public EventBaseModel getEventBaseModel() {
        return eventBaseModel;
    }
}
