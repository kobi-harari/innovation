package com.nils.message;

/**
 * @author tal.maayani on 4/15/2015.
 */
public class InitApplicationMessage extends BaseMessage {
    public InitApplicationMessage(long seed) {
        super(seed);
    }
}
