package com.nils.message;


import com.nils.model.event.EventBaseModel;

/**
 * @author tal.maayani on 5/21/2015.
 */
public class AgentMessage extends BaseMessage {
    EventBaseModel eventBaseModel;

    public AgentMessage(long seed, EventBaseModel eventBaseModel) {
        super(seed);
        this.eventBaseModel = eventBaseModel;
    }

    public EventBaseModel getEventBaseModel() {
        return eventBaseModel;
    }

    public void setEventBaseModel(EventBaseModel eventBaseModel) {
        this.eventBaseModel = eventBaseModel;
    }
}
