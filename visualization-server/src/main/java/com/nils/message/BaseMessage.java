package com.nils.message;

/**
 * @author tal.maayani on 2/1/2015.
 */
public class BaseMessage {
    private final long seed;

    public BaseMessage(long seed) {
        this.seed = seed;
    }

    public long getSeed() {
        return seed;
    }
}
