package com.nils.config;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Tal.Maayani on 1/16/2015.
 */
public class ConfigurationService {
    private static final Logger log = LoggerFactory.getLogger(ConfigurationService.class);
    public static final String DEFAULT_CONFIG_NAME = "application";
    private static Config regularConfig;
    public static ConfigurationService instance = new ConfigurationService();

    private ConfigurationService(){
        this.regularConfig = ConfigFactory.load(DEFAULT_CONFIG_NAME);
    }

    public static String getStringValue(String path){
        try {
            return regularConfig.getString(path);
        }catch(ConfigException.Missing missing){
            log.error("could not find the property" , missing);
            return null;
        }catch(ConfigException.WrongType wrongType){
            log.error("wrong type of the property" , wrongType);
            return null;
        }
    }

    public static List<String> getStringList(String path){
        try {
            return regularConfig.getStringList(path);
        }catch(ConfigException.Missing missing){
            log.error("could not find the property" , missing);
            return null;
        }catch(ConfigException.WrongType wrongType){
            log.error("wrong type of the property" , wrongType);
            return null;
        }
    }

    public static Config getConfig() {
        return regularConfig;
    }

}
