'use strict';
(function(){
/**
 * @ngdoc overview
 * @name nilsApp
 * @description
 * # nilsApp
 *
 * Main module of the application.
 */
var app = angular
  .module('nilsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ui.router',
    'ngSanitize',
    'ngTouch',
    'chart.js',
    'restangular'
  ]);
})();
