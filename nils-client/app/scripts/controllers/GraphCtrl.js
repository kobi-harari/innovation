/**
 * Created by rotem.perets on 5/28/2015.
 */
/**
 * @ngdoc function
 * @name nilsApp.controller:GraphCtrl
 * @description
 * # MainCtrl
 * Controller of the nilsApp
 */
(function () {
    'use strict';

    angular.module('nilsApp')
        .controller('GraphCtrl', GraphCtrl);

    GraphCtrl.$inject = ['restService', '$scope', '$state', '$q'];

    function GraphCtrl(restService, $scope, $state, $q) {

        var vmGraph = this;
        vmGraph.scope = $scope;
        vmGraph.showInfo = false;

        vmGraph.barColours = [{
            fillColor: 'rgba(151, 194, 251, 1)',
            strokeColor: 'rgba(43, 124, 232, 1)',
            highlightFill: 'rgba(251, 126, 129, 1)',
            highlightStroke: 'rgba(250, 10, 16, 1)'
        }];

        vmGraph.lineColours = [{
            fillColor: 'rgba(43, 124, 232, 1)',
            strokeColor: 'rgba(151, 194, 251, 1)',
            highlightFill: 'rgba(151, 194, 251, 1)',
            highlightStroke: 'rgba(43, 124, 232, 1)'
        }];

        var color = 'gray';
        var len = undefined;

        restService.one('applications/' + $state.params.appName + '/graph').get().then(function (data) {
            vmGraph.graphData = data;

            // create a network
            var container = document.getElementById('container');
            var data = {
                nodes: data.nodes,
                edges: data.edges
            };
            var options = {
                nodes: {
                    size: 30,
                    font: {
                        face: 'verdana',
                        size: 32,
                        color: '#000000'
                    },
                    borderWidth: 2,
                    scaling: {
                        label: {
                            drawThreshold: 14,
                            maxVisible: 30
                        }
                    },
                    shadow: {
                      enabled: true
                    }
                },
                edges: {
                    width: 2
                }
            };

            var network = new vis.Network(container, data, options);
            network.on('click', onClick);
        });

        function onClick(obj){
            if(obj.nodes.length = 1){
                var nodeId = obj.nodes[0];
              if (nodeId) {
                var actorClassName;
                var actorPath;
                vmGraph.graphData.nodes.forEach(function (item) {
                  if (item.id == nodeId) {
                    actorClassName = item.className;
                    actorPath = item.path;
                  }
                });
                restService.one('applications/' + $state.params.appName + '/' + actorClassName + '/' + encodeURIComponent(actorPath)).get().then(function (data) {
                  vmGraph.nodeInfo = data.plain();
                  console.log(vmGraph.nodeInfo);
                  vmGraph.showInfo = true;
                });

                restService.one('applications/performance/' + $state.params.appName + '/' + actorClassName + '/Count').get().then(function (data) {
                    vmGraph.countLabels = data.labels;
                    vmGraph.countData = data.data;
                });

                  var min = restService.one('applications/performance/' + $state.params.appName + '/' + actorClassName + '/Mean').get();
                  var max = restService.one('applications/performance/' + $state.params.appName + '/' + actorClassName + '/Max').get();
                  $q.all([min, max]).then(function(data){
                      console.log(data);

                      var meanData = data[0].data;
                      for(var i = 0; i < meanData.length; i++){
                          meanData[i] = Math.round(meanData[i]);
                      }

                      vmGraph.meanLabels = data[0].labels;
                      vmGraph.meanData = [meanData];

                      vmGraph.labels = data[0].labels;
                      vmGraph.data = [meanData, data[1].data];
                  });
              }
            }
        }
    }
})();
