/**
 * @ngdoc function
 * @name nilsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nilsApp
 */
(function () {
  'use strict';

  angular.module('nilsApp')
    .controller('MainCtrl', MainCtrl);

    MainCtrl.$inject = ['$state', 'restService'];

  function MainCtrl($state, restService) {
      restService.all('applications').getList().then(function(data){
          vm.applications = _.sortBy(data,'name');
        }
      );


    var vm = this;
    vm.selectedApp = 0;
    vm.selectedDisplayType = 0;
    vm.title = 'Applications';
    vm.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS'
    ];

    vm.displayTypes = [
      'Graph'
    ];

    vm.selectApp = function(key){
      vm.selectedApp = key;
    };

    vm.changeDisplay = function(app, index){
      var route = (index == 0) ? 'graph' : 'charts';
      $state.go('nils.' + route, {appName: app.key});
    };

    vm.setSelectedDisplayType = function(index){
      vm.selectedDisplayType = index;
    };
  }
})();
