/**
 * Created by rotem.perets on 7/13/2015.
 */
'use strict';
(function(){

    angular.module('nilsApp').directive('nilsModal', NilsModal);

    function NilsModal(){
        return {
            restrict: 'E',
            templateUrl: './views/nilsModal.html',
            scope:{
                info: '=',
                closeMe: '='
            }
        }
    }
})(angular);