/**
 * Created by rotem.perets on 07/13/15.
 */
(function(){
    angular.module('nilsApp').factory('restService', RestService);

    RestService.$inject = ['Restangular', 'configuration'];

    function RestService(Restangular, configuration){
        var restSrv = Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setBaseUrl(configuration.baseUrl);
        });

        return restSrv
    }
})(angular);