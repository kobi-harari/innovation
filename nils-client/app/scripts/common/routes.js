/**
 * Created by rotem.perets on 5/28/2015.
 */
'use strict';

(function(){
  angular
    .module('nilsApp')
    .config(Configure, ['$stateProvider', '$urlRouterProvider']);

  function Configure($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise("/nils");

    $stateProvider
      .state('nils', {
        url: "/nils",
        title: "Main",
        templateUrl: "views/main.html",
        controller: "MainCtrl",
        controllerAs: 'vm'
      })
      .state('nils.graph', {
        url: "/graph/:appName",
        title: "Graph",
        templateUrl: "views/graph.html",
        controller: "GraphCtrl",
        controllerAs: 'vmGraph'
      });
  }
})();

